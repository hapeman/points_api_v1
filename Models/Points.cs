﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace HE_Models2
{
	public class Points
	{

		#region tbl_HistoricPointValues
		public class tbl_HistoricPointValues : List<tbl_HistoricPointValue>
		{
			public tbl_HistoricPointValues() { }
			public tbl_HistoricPointValues(List<tbl_HistoricPointValue> tbl_HistoricPointValues) : base(tbl_HistoricPointValues) { }
		}
		public class tbl_HistoricPointValue
		{
			public int keyHPoint_ID { get; set; }
			public string fkPointID { get; set; }
			public float HPoint_realCurrentValue { get; set; }
			public float HPoint_realRawValue { get; set; }
			public string HPoint_realDisplayValue { get; set; }
			public DateTime HPoint_datetime { get; set; }

		}
		public tbl_HistoricPointValues dt2cls(DataTable oDT, tbl_HistoricPointValues dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_HistoricPointValue dataRec = new tbl_HistoricPointValue()
				{
					keyHPoint_ID = Convert.ToInt32(oDT.Rows[i]["keyHPoint_ID"].ToString()),
					fkPointID = oDT.Rows[i]["fkPointID"].ToString(),
					HPoint_realCurrentValue = float.Parse(oDT.Rows[i]["HPoint_realCurrentValue"].ToString()),
					HPoint_realRawValue = float.Parse(oDT.Rows[i]["HPoint_realRawValue"].ToString()),
					HPoint_realDisplayValue = oDT.Rows[i]["HPoint_realDisplayValue"].ToString(),
					HPoint_datetime = Convert.ToDateTime(oDT.Rows[i]["HPoint_datetime"].ToString())
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}
		#endregion

		#region tbl_Devices

		public class tbl_Devices : List<tbl_Device>
		{
			public tbl_Devices() { }
			public tbl_Devices(List<tbl_Device> tbl_Devices) : base(tbl_Devices) { }
		}
		public class tbl_Device
		{
			public string keyDeviceID { get; set; }
			public string fkDAQ_ID { get; set; }
			public string Dev_strName { get; set; }
			public string fkDevTypeID { get; set; }
			public bool Dev_boolTreeExpanded { get; set; }
			public bool Dev_boolEnabled { get; set; }
			public int Dev_intLoadOrder { get; set; }
			public string fkBusID { get; set; }
			public bool Dev_boolSimulated { get; set; }
		}
		public tbl_Devices dt2cls(DataTable oDT, tbl_Devices dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_Device dataRec = new tbl_Device()
				{

					keyDeviceID = oDT.Rows[i]["keyAsFoundAsLeftID"].ToString(),
					fkDAQ_ID = oDT.Rows[i]["fkDAQ_ID"].ToString(),
					Dev_strName = oDT.Rows[i]["Dev_strName"].ToString(),
					fkDevTypeID = oDT.Rows[i]["fkDevTypeID"].ToString(),
					Dev_boolTreeExpanded = Convert.ToBoolean(oDT.Rows[i]["Dev_boolTreeExpanded"].ToString()),
					Dev_boolEnabled = Convert.ToBoolean(oDT.Rows[i]["Dev_boolEnabled"].ToString()),
					Dev_intLoadOrder = Convert.ToInt32(oDT.Rows[i]["Dev_intLoadOrder"].ToString()),
					fkBusID = oDT.Rows[i]["fkBusID"].ToString(),
					Dev_boolSimulated = Convert.ToBoolean(oDT.Rows[i]["Dev_boolSimulated"].ToString())
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_PointUniqueValues

		public class tbl_PointUniqueValues : List<tbl_PointUniqueValue>
		{
			public tbl_PointUniqueValues() { }
			public tbl_PointUniqueValues(List<tbl_PointUniqueValue> tbl_PointUniqueValues) : base(tbl_PointUniqueValues) { }
		}
		public class tbl_PointUniqueValue
		{
			public string keyPointUV_ID { get; set; }
			public string fkPointID { get; set; }
			public string fkPUDF_ID { get; set; }
			public string PointUV_strValue { get; set; }
		}
		public tbl_PointUniqueValues dt2cls(DataTable oDT, tbl_PointUniqueValues dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_PointUniqueValue dataRec = new tbl_PointUniqueValue()
				{

					keyPointUV_ID = oDT.Rows[i]["keyAsFoundAsLeftID"].ToString(),
					fkPointID = oDT.Rows[i]["fkPointID"].ToString(),
					fkPUDF_ID = oDT.Rows[i]["fkPUDF_ID"].ToString(),
					PointUV_strValue = oDT.Rows[i]["PointUV_strValue"].ToString()
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_Points

		public class tbl_Points : List<tbl_Point>
		{
			public tbl_Points() { }
			public tbl_Points(List<tbl_Point> tbl_Points) : base(tbl_Points) { }
		}
		public class tbl_Point
		{
			
			//References		
					//NA
			//Parent
					//NA
			//Owner
					//NA
			//Device
					//NA
					
		
			//Values - for gets/sets
			public float Point_realRawValue { get; set; }
			public float Point_realCurrentValue { get; set; }
			public float Point_singleFutureValue { get; set; }
			public bool Point_boolHasFutureValue { get; set; }

			//Normal Fields from DB
			public string keyPointID { get; set; }
			public string fkUnitsID { get; set; }
			public string fkTranslationTypeID { get; set; }		
			public string fkDeviceID { get; set; }

			public bool Point_boolEnabled { get; set; }
			public int? Point_intIndex { get; set; }
			public string Point_strName { get; set; }
			public string Point_strDescription { get; set; }
			public int? Point_intDisplayDecimals { get; set; }
			public int? Point_intRecordDecimals { get; set; }
			public string Point_strFormulaText { get; set; }
			public bool Point_boolControlPoint { get; set; }
			public bool Point_boolIsBoolean { get; set; }
			public bool Point_boolTrackStats { get; set; }
			public bool Point_boolArrayPoint { get; set; }
			public string Point_strDisplayName { get; set; }
			public string Point_strNotes { get; set; }
			public string Point_strTag { get; set; }
			public int? Point_intCalInterval { get; set; }

			//Slope && Offsets
			public float Point_realHardwareSlope { get; set; }
			public float Point_realHardwareOffset { get; set; }
			public float Point_realTransducerSlope { get; set; }
			public float Point_realTransducerOffset { get; set; }
			
			//Other Properties
			public float Point_realDisplayValue { get; set; }
			public DateTime Point_dateUpdateTime { get; set; }
			
			
		}
		public tbl_Points dt2cls(DataTable oDT, tbl_Points dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_Point dataRec = new tbl_Point()
				{

					keyPointID = oDT.Rows[i]["keyPointID"].ToString(),
					fkDeviceID = oDT.Rows[i]["fkDeviceID"].ToString(),
					Point_boolEnabled = Convert.ToBoolean(oDT.Rows[i]["Point_boolEnabled"].ToString()),
					Point_intIndex = Convert.ToInt32(oDT.Rows[i]["Point_intIndex"].ToString()),
					fkUnitsID = oDT.Rows[i]["fkUnitsID"].ToString(),
					Point_strName = oDT.Rows[i]["Point_strName"].ToString(),
					Point_strDescription = oDT.Rows[i]["Point_strDescription"].ToString(),
					fkTranslationTypeID = oDT.Rows[i]["fkTranslationTypeID"].ToString(),
					Point_intDisplayDecimals = Convert.ToInt32(oDT.Rows[i]["Point_intDisplayDecimals"].ToString()),
					Point_intRecordDecimals = Convert.ToInt32(oDT.Rows[i]["Point_intRecordDecimals"].ToString()),
					Point_strFormulaText = oDT.Rows[i]["Point_strFormulaText"].ToString(),
					Point_boolControlPoint = Convert.ToBoolean(oDT.Rows[i]["Point_boolControlPoint"].ToString()),
					Point_boolIsBoolean = Convert.ToBoolean(oDT.Rows[i]["Point_boolIsBoolean"].ToString()),
					Point_boolTrackStats = Convert.ToBoolean(oDT.Rows[i]["Point_boolTrackStats"].ToString()),
					Point_boolArrayPoint = Convert.ToBoolean(oDT.Rows[i]["Point_boolArrayPoint"].ToString()),
					Point_strDisplayName = oDT.Rows[i]["Point_strDisplayName"].ToString(),
					Point_strNotes = oDT.Rows[i]["Point_strNotes"].ToString(),
					Point_strTag = oDT.Rows[i]["Point_strTag"].ToString(),
					Point_intCalInterval = Convert.ToInt32(oDT.Rows[i]["Point_intCalInterval"].ToString()),
					Point_realHardwareSlope = float.Parse(oDT.Rows[i]["Point_realHardwareSlope"].ToString()),
					Point_realHardwareOffset = float.Parse(oDT.Rows[i]["Point_realHardwareOffset"].ToString()),
					Point_realTransducerSlope = float.Parse(oDT.Rows[i]["Point_realTransducerSlope"].ToString()),
					Point_realTransducerOffset = float.Parse(oDT.Rows[i]["Point_realTransducerOffset"].ToString()),
					Point_realRawValue = float.Parse(oDT.Rows[i]["Point_realRawValue"].ToString()),
					Point_realCurrentValue = float.Parse(oDT.Rows[i]["Point_realCurrentValue"].ToString()),
					Point_realDisplayValue = float.Parse(oDT.Rows[i]["Point_realDisplayValue"].ToString()),
					Point_dateUpdateTime = Convert.ToDateTime(oDT.Rows[i]["Point_dateUpdateTime"].ToString()),
					Point_singleFutureValue = float.Parse(oDT.Rows[i]["Point_singleFutureValue"].ToString()),
					Point_boolHasFutureValue = Convert.ToBoolean(oDT.Rows[i]["Point_boolHasFutureValue"].ToString())
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_PointRanges

		public class tbl_PointRanges : List<tbl_PointRange>
		{
			public tbl_PointRanges() { }
			public tbl_PointRanges(List<tbl_PointRange> tbl_PointRanges) : base(tbl_PointRanges) { }
		}
		public class tbl_PointRange
		{
			public string keyPointRangeID { get; set; }
			public string fkPointID { get; set; }
			public string fkRangeTypeID { get; set; }
			public float PointRange_fltMin { get; set; }
			public float PointRange_fltMax { get; set; }
			public bool PointRange_boolDisabled { get; set; }

		}
		public tbl_PointRanges dt2cls(DataTable oDT, tbl_PointRanges dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_PointRange dataRec = new tbl_PointRange()
				{

					keyPointRangeID = oDT.Rows[i]["keyPointRangeID"].ToString(),
					fkPointID = oDT.Rows[i]["fkPointID"].ToString(),
					fkRangeTypeID = oDT.Rows[i]["fkRangeTypeID"].ToString(),
					PointRange_fltMin = float.Parse(oDT.Rows[i]["PointRange_fltMin"].ToString()),
					PointRange_fltMax = float.Parse(oDT.Rows[i]["PointRange_fltMax"].ToString()),
					PointRange_boolDisabled = Convert.ToBoolean(oDT.Rows[i]["PointRange_boolDisabled"].ToString()),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_DeviceUniqueValues

		public class tbl_DeviceUniqueValues : List<tbl_DeviceUniqueValue>
		{
			public tbl_DeviceUniqueValues() { }
			public tbl_DeviceUniqueValues(List<tbl_DeviceUniqueValue> tbl_DeviceUniqueValues) : base(tbl_DeviceUniqueValues) { }
		}
		public class tbl_DeviceUniqueValue
		{
			public string keyDevUV_ID { get; set; }
			public string fkDeviceID { get; set; }
			public string fkDUDF_ID { get; set; }
			public string DevUV_strValue { get; set; }
		}
		public tbl_DeviceUniqueValues dt2cls(DataTable oDT, tbl_DeviceUniqueValues dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_DeviceUniqueValue dataRec = new tbl_DeviceUniqueValue()
				{

					keyDevUV_ID = oDT.Rows[i]["keyDevUV_ID"].ToString(),
					fkDeviceID = oDT.Rows[i]["fkDeviceID"].ToString(),
					fkDUDF_ID = oDT.Rows[i]["fkDUDF_ID"].ToString(),
					DevUV_strValue = oDT.Rows[i]["DevUV_strValue"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_DAQs

		public class tbl_DAQs : List<tbl_DAQ>
		{
			public tbl_DAQs() { }
			public tbl_DAQs(List<tbl_DAQ> tbl_DAQs) : base(tbl_DAQs) { }
		}
		public class tbl_DAQ
		{
			public string keyDAQ_ID { get; set; }
			public string DAQ_strName { get; set; }
			public string DAQ_strLocation { get; set; }
			public bool DAQ_boolTreeExpanded { get; set; }
			public bool DAQ_boolEnabled { get; set; }
		}
		public tbl_DAQs dt2cls(DataTable oDT, tbl_DAQs dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_DAQ dataRec = new tbl_DAQ()
				{
					keyDAQ_ID = oDT.Rows[i]["keyDAQ_ID"].ToString(),
					DAQ_strName = oDT.Rows[i]["DAQ_strName"].ToString(),
					DAQ_strLocation = oDT.Rows[i]["DAQ_strLocation"].ToString(),
					DAQ_boolTreeExpanded = Convert.ToBoolean(oDT.Rows[i]["DAQ_boolTreeExpanded"].ToString()),
					DAQ_boolEnabled = Convert.ToBoolean(oDT.Rows[i]["DAQ_boolEnabled"].ToString()),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_Buses

		public class tbl_Buses : List<tbl_Bus>
		{
			public tbl_Buses() { }
			public tbl_Buses(List<tbl_Bus> tbl_Buses) : base(tbl_Buses) { }
		}
		public class tbl_Bus
		{
			public string keyBusID { get; set; }
			public string Bus_strName { get; set; }
			public string fkDAQ_ID { get; set; }
			public string fkBusTypeID { get; set; }
			public string Bus_strNotes { get; set; }
		}
		public tbl_Buses dt2cls(DataTable oDT, tbl_Buses dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_Bus dataRec = new tbl_Bus()
				{
					keyBusID = oDT.Rows[i]["keyDAQ_ID"].ToString(),
					Bus_strName = oDT.Rows[i]["DAQ_strName"].ToString(),
					fkDAQ_ID = oDT.Rows[i]["DAQ_strLocation"].ToString(),
					fkBusTypeID = oDT.Rows[i]["DAQ_boolTreeExpanded"].ToString(),
					Bus_strNotes = oDT.Rows[i]["DAQ_boolEnabled"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_Versions

		public class ref_Versions : List<ref_Version>
		{
			public ref_Versions() { }
			public ref_Versions(List<ref_Version> ref_Versions) : base(ref_Versions) { }
		}
		public class ref_Version
		{
			public int? keyVersionID { get; set; }
			public string Version_strIntelimationVersion { get; set; }
			public DateTime Version_sdtInstallDate { get; set; }
			public string Version_strNotes { get; set; }
		}
		public ref_Versions dt2cls(DataTable oDT, ref_Versions dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_Version dataRec = new ref_Version()
				{
					keyVersionID = Convert.ToInt32(oDT.Rows[i]["keyVersionID"].ToString()),
					Version_strIntelimationVersion = oDT.Rows[i]["Version_strIntelimationVersion"].ToString(),
					Version_sdtInstallDate = Convert.ToDateTime(oDT.Rows[i]["Version_sdtInstallDate"].ToString()),
					Version_strNotes = oDT.Rows[i]["Version_strNotes"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_Units

		public class ref_Units : List<ref_Unit>
		{
			public ref_Units() { }
			public ref_Units(List<ref_Unit> ref_Units) : base(ref_Units) { }
		}
		public class ref_Unit
		{
			public string keyUnitID { get; set; }
			public string Unit_strUnits { get; set; }
			public bool Unit_boolTreeExpanded { get; set; }
		}
		public ref_Units dt2cls(DataTable oDT, ref_Units dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_Unit dataRec = new ref_Unit()
				{
					keyUnitID = oDT.Rows[i]["keyUnitID"].ToString(),
					Unit_strUnits = oDT.Rows[i]["Unit_strUnits"].ToString(),
					Unit_boolTreeExpanded = Convert.ToBoolean(oDT.Rows[i]["Unit_boolTreeExpanded"].ToString()),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_TranslationValues

		public class ref_TranslationValues : List<ref_TranslationValue>
		{
			public ref_TranslationValues() { }
			public ref_TranslationValues(List<ref_TranslationValue> ref_TranslationValues) : base(ref_TranslationValues) { }
		}
		public class ref_TranslationValue
		{
			public string keyTranslationValueID { get; set; }
			public string fkTranslationTypeID { get; set; }
			public string TranValue_Value { get; set; }
			public string TranValue_Translation { get; set; }
		}
		public ref_TranslationValues dt2cls(DataTable oDT, ref_TranslationValues dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_TranslationValue dataRec = new ref_TranslationValue()
				{
					keyTranslationValueID = oDT.Rows[i]["keyTranslationValueID"].ToString(),
					fkTranslationTypeID = oDT.Rows[i]["fkTranslationTypeID"].ToString(),
					TranValue_Value = oDT.Rows[i]["TranValue_Value"].ToString(),
					TranValue_Translation = oDT.Rows[i]["TranValue_Translation"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_TranslationTypes

		public class ref_TranslationTypes : List<ref_TranslationType>
		{
			public ref_TranslationTypes() { }
			public ref_TranslationTypes(List<ref_TranslationType> ref_TranslationTypes) : base(ref_TranslationTypes) { }
		}
		public class ref_TranslationType
		{
			public string keyTranslationTypeID { get; set; }
			public string TransType_Name { get; set; }
			public string TransType_Description { get; set; }
		}
		public ref_TranslationTypes dt2cls(DataTable oDT, ref_TranslationTypes dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_TranslationType dataRec = new ref_TranslationType()
				{
					keyTranslationTypeID = oDT.Rows[i]["keyTranslationTypeID"].ToString(),
					TransType_Name = oDT.Rows[i]["TransType_Name"].ToString(),
					TransType_Description = oDT.Rows[i]["TransType_Description"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_Recorders

		public class ref_Recorders : List<ref_Recorder>
		{
			public ref_Recorders() { }
			public ref_Recorders(List<ref_Recorder> ref_Recorders) : base(ref_Recorders) { }
		}
		public class ref_Recorder
		{
			public string keyRecorderID { get; set; }
			public string Recorder_strName { get; set; }
			public int? Recorder_intBit { get; set; }
		}
		public ref_Recorders dt2cls(DataTable oDT, ref_Recorders dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_Recorder dataRec = new ref_Recorder()
				{
					keyRecorderID = oDT.Rows[i]["keyRecorderID"].ToString(),
					Recorder_strName = oDT.Rows[i]["Recorder_strName"].ToString(),
					Recorder_intBit = Convert.ToInt32(oDT.Rows[i]["Recorder_intBit"].ToString()),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_RangeTypes

		public class ref_RangeTypes : List<ref_RangeType>
		{
			public ref_RangeTypes() { }
			public ref_RangeTypes(List<ref_RangeType> ref_RangeTypes) : base(ref_RangeTypes) { }
		}
		public class ref_RangeType
		{
			public string keyRangeTypeID { get; set; }
			public int? RangeType_intOrder { get; set; }
			public string RangeType_strName { get; set; }
			public string RangeType_strBelowMinColor { get; set; }
			public string RangeType_strInRangeColor { get; set; }
			public string RangeType_strOverMaxColor { get; set; }
			public bool RangeType_boolTreeExpanded { get; set; }
			public bool RangeType_boolShutDownRange { get; set; }
			public bool RangeType_boolSensorLimitRange { get; set; }
			public bool RangeType_boolDisabled { get; set; }
			public string RangeType_strInRangeForeColor { get; set; }
			public string RangeType_strInRangeBackColor { get; set; }
			public string RangeType_strUnderRangeForeColor { get; set; }
			public string RangeType_strUnderRangeBackColor { get; set; }
			public string RangeType_strOverRangeForeColor { get; set; }
			public string RangeType_strOverRangeBackColor { get; set; }
		}
		public ref_RangeTypes dt2cls(DataTable oDT, ref_RangeTypes dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_RangeType dataRec = new ref_RangeType()
				{
					keyRangeTypeID = oDT.Rows[i]["keyRangeTypeID"].ToString(),
					RangeType_intOrder = Convert.ToInt32(oDT.Rows[i]["RangeType_intOrder"].ToString()),
					RangeType_strName = oDT.Rows[i]["RangeType_strName"].ToString(),
					RangeType_strBelowMinColor = oDT.Rows[i]["RangeType_strBelowMinColor"].ToString(),
					RangeType_strInRangeColor = oDT.Rows[i]["RangeType_strInRangeColor"].ToString(),
					RangeType_strOverMaxColor = oDT.Rows[i]["RangeType_strOverMaxColor"].ToString(),
					RangeType_boolTreeExpanded = Convert.ToBoolean(oDT.Rows[i]["RangeType_boolTreeExpanded"].ToString()),
					RangeType_boolShutDownRange = Convert.ToBoolean(oDT.Rows[i]["RangeType_boolShutDownRange"].ToString()),
					RangeType_boolSensorLimitRange = Convert.ToBoolean(oDT.Rows[i]["RangeType_boolSensorLimitRange"].ToString()),
					RangeType_boolDisabled = Convert.ToBoolean(oDT.Rows[i]["RangeType_boolDisabled"].ToString()),
					RangeType_strInRangeForeColor = oDT.Rows[i]["RangeType_strInRangeForeColor"].ToString(),
					RangeType_strInRangeBackColor = oDT.Rows[i]["RangeType_strInRangeBackColor"].ToString(),
					RangeType_strUnderRangeForeColor = oDT.Rows[i]["RangeType_strUnderRangeForeColor"].ToString(),
					RangeType_strUnderRangeBackColor = oDT.Rows[i]["RangeType_strUnderRangeBackColor"].ToString(),
					RangeType_strOverRangeForeColor = oDT.Rows[i]["RangeType_strOverRangeForeColor"].ToString(),
					RangeType_strOverRangeBackColor = oDT.Rows[i]["RangeType_strOverRangeBackColor"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_RangePresets

		public class ref_RangePresets : List<ref_RangePreset>
		{
			public ref_RangePresets() { }
			public ref_RangePresets(List<ref_RangePreset> ref_RangePresets) : base(ref_RangePresets) { }
		}
		public class ref_RangePreset
		{
			public string keyRangePresetID { get; set; }
			public string fkRangeTypeID { get; set; }
			public string fkUnitID { get; set; }
			public string RangePreset_strName { get; set; }
			public float RangePreset_fltMin { get; set; }
			public float RangePreset_fltMax { get; set; }
			public bool RangePreset_boolDefault { get; set; }
			public bool RangePreset_boolTreeExpanded { get; set; }
		}
		public ref_RangePresets dt2cls(DataTable oDT, ref_RangePresets dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_RangePreset dataRec = new ref_RangePreset()
				{
					keyRangePresetID = oDT.Rows[i]["keyRangePresetID"].ToString(),
					fkRangeTypeID = oDT.Rows[i]["fkRangeTypeID"].ToString(),
					fkUnitID = oDT.Rows[i]["fkUnitID"].ToString(),
					RangePreset_strName = oDT.Rows[i]["RangePreset_strName"].ToString(),
					RangePreset_fltMin = float.Parse(oDT.Rows[i]["RangePreset_fltMin"].ToString()),
					RangePreset_fltMax = float.Parse(oDT.Rows[i]["RangePreset_fltMax"].ToString()),
					RangePreset_boolDefault = Convert.ToBoolean(oDT.Rows[i]["RangePreset_boolDefault"].ToString()),
					RangePreset_boolTreeExpanded = Convert.ToBoolean(oDT.Rows[i]["RangePreset_boolTreeExpanded"].ToString()),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_PointUDFs

		public class ref_PointUDFs : List<ref_PointUDF>
		{
			public ref_PointUDFs() { }
			public ref_PointUDFs(List<ref_PointUDF> ref_PointUDFs) : base(ref_PointUDFs) { }
		}
		public class ref_PointUDF
		{
			public string keyPUDF_ID { get; set; }
			public string fkDevTypeID { get; set; }
			public string PUDF_strFieldName { get; set; }
			public string PUDF_strDescription { get; set; }
			public string PUDF_strDefault { get; set; }
			public bool PUDF_boolNonDeveloper { get; set; }
			public string PUDF_strFieldDef { get; set; }
		}
		public ref_PointUDFs dt2cls(DataTable oDT, ref_PointUDFs dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_PointUDF dataRec = new ref_PointUDF()
				{
					keyPUDF_ID = oDT.Rows[i]["keyPUDF_ID"].ToString(),
					fkDevTypeID = oDT.Rows[i]["fkDevTypeID"].ToString(),
					PUDF_strFieldName = oDT.Rows[i]["PUDF_strFieldName"].ToString(),
					PUDF_strDescription = oDT.Rows[i]["PUDF_strDescription"].ToString(),
					PUDF_strDefault = oDT.Rows[i]["RangePreset_fltMin"].ToString(),
					PUDF_boolNonDeveloper = Convert.ToBoolean(oDT.Rows[i]["RangePreset_fltMax"].ToString()),
					PUDF_strFieldDef = oDT.Rows[i]["RangePreset_boolDefault"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_DriverTypes

		public class ref_DriverTypes : List<ref_DriverType>
		{
			public ref_DriverTypes() { }
			public ref_DriverTypes(List<ref_DriverType> ref_DriverTypes) : base(ref_DriverTypes) { }
		}
		public class ref_DriverType
		{
			public string keyDriverTypeID { get; set; }
			public string DriverTypestrValue { get; set; }

		}
		public ref_DriverTypes dt2cls(DataTable oDT, ref_DriverTypes dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_DriverType dataRec = new ref_DriverType()
				{
					keyDriverTypeID = oDT.Rows[i]["keyPUDF_ID"].ToString(),
					DriverTypestrValue = oDT.Rows[i]["fkDevTypeID"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_DeviceUDFs

		public class ref_DeviceUDFs : List<ref_DeviceUDF>
		{
			public ref_DeviceUDFs() { }
			public ref_DeviceUDFs(List<ref_DeviceUDF> ref_DeviceUDFs) : base(ref_DeviceUDFs) { }
		}
		public class ref_DeviceUDF
		{
			public string keyDUDF_ID { get; set; }
			public string fkDevTypeID { get; set; }
			public string DUDF_strFieldName { get; set; }
			public string DUDF_strDescription { get; set; }
			public string DUDF_strDefault { get; set; }
			public bool DUDF_boolNonDeveloper { get; set; }
			public string DUDF_strFieldDef { get; set; }
		}
		public ref_DeviceUDFs dt2cls(DataTable oDT, ref_DeviceUDFs dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_DeviceUDF dataRec = new ref_DeviceUDF()
				{
					keyDUDF_ID = oDT.Rows[i]["keyDUDF_ID"].ToString(),
					fkDevTypeID = oDT.Rows[i]["fkDevTypeID"].ToString(),
					DUDF_strFieldName = oDT.Rows[i]["DUDF_strFieldName"].ToString(),
					DUDF_strDescription = oDT.Rows[i]["DUDF_strDescription"].ToString(),
					DUDF_strDefault = oDT.Rows[i]["DUDF_strDefault"].ToString(),
					DUDF_boolNonDeveloper = Convert.ToBoolean(oDT.Rows[i]["DUDF_boolNonDeveloper"].ToString()),
					DUDF_strFieldDef = oDT.Rows[i]["DUDF_strFieldDef"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}
		#endregion

		#region ref_DeviceTypes

		public class ref_DeviceTypes : List<ref_DeviceType>
		{
			public ref_DeviceTypes() { }
			public ref_DeviceTypes(List<ref_DeviceType> ref_DeviceTypes) : base(ref_DeviceTypes) { }
		}
		public class ref_DeviceType
		{
			public string keyDevTypeID { get; set; }
			public string DevType_strName { get; set; }
			public string fkDriverTypeID { get; set; }
			public int? DevType_intLoadOrder { get; set; }
			public string DevType_strEl_ClassName { get; set; }
			public string DevType_strWIZ_ClassName { get; set; }
		}
		public ref_DeviceTypes dt2cls(DataTable oDT, ref_DeviceTypes dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_DeviceType dataRec = new ref_DeviceType()
				{
					keyDevTypeID = oDT.Rows[i]["keyDevTypeID"].ToString(),
					DevType_strName = oDT.Rows[i]["DevType_strName"].ToString(),
					fkDriverTypeID = oDT.Rows[i]["fkDriverTypeID"].ToString(),
					DevType_intLoadOrder = Convert.ToInt32(oDT.Rows[i]["DevType_intLoadOrder"].ToString()),
					DevType_strEl_ClassName = oDT.Rows[i]["DevType_strEl_ClassName"].ToString(),
					DevType_strWIZ_ClassName = oDT.Rows[i]["DevType_strWIZ_ClassName"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_BusTypes

		public class ref_BusTypes : List<ref_BusType>
		{
			public ref_BusTypes() { }
			public ref_BusTypes(List<ref_BusType> ref_BusTypes) : base(ref_BusTypes) { }
		}
		public class ref_BusType
		{
			public string keyBusTypeID { get; set; }
			public string BusType_strName { get; set; }
			public string BusType_strDescription { get; set; }
		}
		public ref_BusTypes dt2cls(DataTable oDT, ref_BusTypes dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_BusType dataRec = new ref_BusType()
				{
					keyBusTypeID = oDT.Rows[i]["keyBusTypeID"].ToString(),
					BusType_strName = oDT.Rows[i]["BusType_strName"].ToString(),
					BusType_strDescription = oDT.Rows[i]["BusType_strDescription"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region lst_RecorderPoints

		public class lst_RecorderPoints : List<lst_RecorderPoint>
		{
			public lst_RecorderPoints() { }
			public lst_RecorderPoints(List<lst_RecorderPoint> lst_RecorderPoints) : base(lst_RecorderPoints) { }
		}
		public class lst_RecorderPoint
		{
			public string keyRecorderPointID { get; set; }
			public string fkRecorderID { get; set; }
			public string fkPointID { get; set; }
		}
		public lst_RecorderPoints dt2cls(DataTable oDT, lst_RecorderPoints dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				lst_RecorderPoint dataRec = new lst_RecorderPoint()
				{
					keyRecorderPointID = oDT.Rows[i]["keyRecorderPointID"].ToString(),
					fkRecorderID = oDT.Rows[i]["fkRecorderID"].ToString(),
					fkPointID = oDT.Rows[i]["fkPointID"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_HistoricPointUniqueValues

		public class tbl_HistoricPointUniqueValues : List<tbl_HistoricPointUniqueValue>
		{
			public tbl_HistoricPointUniqueValues() { }
			public tbl_HistoricPointUniqueValues(List<tbl_HistoricPointUniqueValue> tbl_HistoricPointUniqueValues) : base(tbl_HistoricPointUniqueValues) { }
		}
		public class tbl_HistoricPointUniqueValue
		{
			public string keyHistoricPointUniqueValueID { get; set; }
			public string fkHistroicPointDataID { get; set; }
			public string fkPointID { get; set; }
			public string fkPUDF_ID { get; set; }
			public string HistoricPointUniqueValue_strValue { get; set; }
		}
		public tbl_HistoricPointUniqueValues dt2cls(DataTable oDT, tbl_HistoricPointUniqueValues dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_HistoricPointUniqueValue dataRec = new tbl_HistoricPointUniqueValue()
				{
					keyHistoricPointUniqueValueID = oDT.Rows[i]["keyPUDF_ID"].ToString(),
					fkHistroicPointDataID = oDT.Rows[i]["fkDevTypeID"].ToString(),
					fkPointID = oDT.Rows[i]["PUDF_strFieldName"].ToString(),
					fkPUDF_ID = oDT.Rows[i]["PUDF_strDescription"].ToString(),
					HistoricPointUniqueValue_strValue = oDT.Rows[i]["RangePreset_fltMin"].ToString(),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_HistoricPointDatas

		public class tbl_HistoricPointDatas : List<tbl_HistoricPointData>
		{
			public tbl_HistoricPointDatas() { }
			public tbl_HistoricPointDatas(List<tbl_HistoricPointData> tbl_HistoricPointDatas) : base(tbl_HistoricPointDatas) { }
		}
		public class tbl_HistoricPointData
		{
			public string keyHisoricPointData { get; set; }
			public string fkPointID { get; set; }
			public string fkCalibrationListID { get; set; }
			public DateTime HistoricPointData_dateDateRetired { get; set; }
			public string fkDeviceID { get; set; }
			public bool HistoricPointData_boolEnabled { get; set; }
			public int? HistoricPointData_intIndex { get; set; }
			public string fkUnitsID { get; set; }
			public string HistoricPointData_strName { get; set; }
			public string HistoricPointData_strDescription { get; set; }
			public string fkTranslationTypeID { get; set; }
			public int? HistoricPointData_intDisplayDecimals { get; set; }
			public int? HistoricPointData_intRecordDecimals { get; set; }
			public string HistoricPointData_strFormulaText { get; set; }
			public bool HistoricPointData_boolControlPoint { get; set; }
			public bool HistoricPointData_boolIsBoolean { get; set; }
			public bool HistoricPointData_boolTrackStats { get; set; }
			public bool HistoricPointData_boolArrayPoint { get; set; }
			public string HistoricPointData_strDisplayName { get; set; }
			public string HistoricPointData_strNotes { get; set; }
			public string HistoricPointData_strTag { get; set; }
			public int? HistoricPointData_intCalInterval { get; set; }

		}
		public tbl_HistoricPointDatas dt2cls(DataTable oDT, tbl_HistoricPointDatas dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_HistoricPointData dataRec = new tbl_HistoricPointData()
				{

					keyHisoricPointData = oDT.Rows[i]["keyHisoricPointData"].ToString(),
					fkPointID = oDT.Rows[i]["fkPointID"].ToString(),
					fkCalibrationListID = oDT.Rows[i]["fkCalibrationListID"].ToString(),
					HistoricPointData_dateDateRetired = Convert.ToDateTime(oDT.Rows[i]["HistoricPointData_dateDateRetired"].ToString()),
					fkDeviceID = oDT.Rows[i]["fkDeviceID"].ToString(),
					HistoricPointData_boolEnabled = Convert.ToBoolean(oDT.Rows[i]["HistoricPointData_boolEnabled"].ToString()),
					HistoricPointData_intIndex = Convert.ToInt32(oDT.Rows[i]["HistoricPointData_intIndex"].ToString()),
					fkUnitsID = oDT.Rows[i]["fkUnitsID"].ToString(),
					HistoricPointData_strName = oDT.Rows[i]["HistoricPointData_strName"].ToString(),
					HistoricPointData_strDescription = oDT.Rows[i]["HistoricPointData_strDescription"].ToString(),
					fkTranslationTypeID = oDT.Rows[i]["fkTranslationTypeID"].ToString(),
					HistoricPointData_intDisplayDecimals = Convert.ToInt32(oDT.Rows[i]["HistoricPointData_intDisplayDecimals"].ToString()),
					HistoricPointData_intRecordDecimals = Convert.ToInt32(oDT.Rows[i]["HistoricPointData_intRecordDecimals"].ToString()),
					HistoricPointData_strFormulaText = oDT.Rows[i]["HistoricPointData_strFormulaText"].ToString(),
					HistoricPointData_boolControlPoint = Convert.ToBoolean(oDT.Rows[i]["HistoricPointData_boolControlPoint"].ToString()),
					HistoricPointData_boolIsBoolean = Convert.ToBoolean(oDT.Rows[i]["HistoricPointData_boolIsBoolean"].ToString()),
					HistoricPointData_boolTrackStats = Convert.ToBoolean(oDT.Rows[i]["HistoricPointData_boolTrackStats"].ToString()),
					HistoricPointData_boolArrayPoint = Convert.ToBoolean(oDT.Rows[i]["HistoricPointData_boolArrayPoint"].ToString()),
					HistoricPointData_strDisplayName = oDT.Rows[i]["HistoricPointData_strDisplayName"].ToString(),
					HistoricPointData_strNotes = oDT.Rows[i]["HistoricPointData_strNotes"].ToString(),
					HistoricPointData_strTag = oDT.Rows[i]["HistoricPointData_strTag"].ToString(),
					HistoricPointData_intCalInterval = Convert.ToInt32(oDT.Rows[i]["HistoricPointData_intCalInterval"].ToString())
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_CalibrationRows

		public class tbl_CalibrationRows : List<tbl_CalibrationRow>
		{
			public tbl_CalibrationRows() { }
			public tbl_CalibrationRows(List<tbl_CalibrationRow> tbl_CalibrationRows) : base(tbl_CalibrationRows) { }
		}
		public class tbl_CalibrationRow
		{
			public string keyCalibrationRowID { get; set; }
			public string fkCalibrationListID { get; set; }
			public string fkDataSummaryGroupID { get; set; }
			public int? fkCalibrationValuesTypeID { get; set; }

		}
		public tbl_CalibrationRows dt2cls(DataTable oDT, tbl_CalibrationRows dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_CalibrationRow dataRec = new tbl_CalibrationRow()
				{

					keyCalibrationRowID = oDT.Rows[i]["keyCalibrationRowID"].ToString(),
					fkCalibrationListID = oDT.Rows[i]["fkCalibrationListID"].ToString(),
					fkDataSummaryGroupID = oDT.Rows[i]["fkDataSummaryGroupID"].ToString(),
					fkCalibrationValuesTypeID = Convert.ToInt32(oDT.Rows[i]["fkCalibrationValuesTypeID"].ToString()),

				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region tbl_CalibrationLists

		public class tbl_CalibrationLists : List<tbl_CalibrationList>
		{
			public tbl_CalibrationLists() { }
			public tbl_CalibrationLists(List<tbl_CalibrationList> tbl_CalibrationLists) : base(tbl_CalibrationLists) { }
		}
		public class tbl_CalibrationList
		{
			public string keyCalibrationListID { get; set; }
			public string fkPointID { get; set; }
			public DateTime CalibrationList_dateDateCreated { get; set; }
			public DateTime CalibrationList_dateDateRetired { get; set; }
			public bool CalibrationList_Active { get; set; }

		}
		public tbl_CalibrationLists dt2cls(DataTable oDT, tbl_CalibrationLists dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				tbl_CalibrationList dataRec = new tbl_CalibrationList()
				{

					keyCalibrationListID = oDT.Rows[i]["keyCalibrationListID"].ToString(),
					fkPointID = oDT.Rows[i]["fkPointID"].ToString(),
					CalibrationList_dateDateCreated = Convert.ToDateTime(oDT.Rows[i]["CalibrationList_dateDateCreated"].ToString()),
					CalibrationList_dateDateRetired = Convert.ToDateTime(oDT.Rows[i]["CalibrationList_dateDateRetired"].ToString()),
					CalibrationList_Active = Convert.ToBoolean(oDT.Rows[i]["CalibrationList_Active"].ToString()),
				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion

		#region ref_CalibrationValuesTypes

		public class ref_CalibrationValuesTypes : List<ref_CalibrationValuesType>
		{
			public ref_CalibrationValuesTypes() { }
			public ref_CalibrationValuesTypes(List<ref_CalibrationValuesType> ref_CalibrationValuesTypes) : base(ref_CalibrationValuesTypes) { }
		}
		public class ref_CalibrationValuesType
		{
			public int? keyCalibrationValuesTypeID { get; set; }
			public string CalibrationValuesTypes_strName { get; set; }
			public string CalibrationValuesTypes_strDescription { get; set; }

		}
		public ref_CalibrationValuesTypes dt2cls(DataTable oDT, ref_CalibrationValuesTypes dataRecs)
		{
			for (int i = 0; i < oDT.Rows.Count; i++)
			{
				ref_CalibrationValuesType dataRec = new ref_CalibrationValuesType()
				{

					keyCalibrationValuesTypeID = Convert.ToInt32(oDT.Rows[i]["keyCalibrationListID"].ToString()),
					CalibrationValuesTypes_strName = oDT.Rows[i]["fkPointID"].ToString(),
					CalibrationValuesTypes_strDescription = oDT.Rows[i]["CalibrationList_dateDateCreated"].ToString(),

				};
				dataRecs.Add(dataRec);
			}
			return dataRecs;
		}

		#endregion
	}
}
