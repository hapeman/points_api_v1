﻿using System.Data;
using System.Net;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using System;

//using System.Windows.Forms;
//using System.Web.Script.Serialization;
//using System.Net.Http.Formatting;

namespace HE_Models2
{
    public class apiFunctions
	{
		#region getData
		public System.Data.DataSet getData(string sURI, string sAppToken, string sUserToken)
        {
            DataSet oDS = new DataSet();

            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    XmlDocument xmlResponse = (XmlDocument)JsonConvert.DeserializeXmlNode(sData, "Tables");
                    string sXML = xmlResponse.InnerXml.ToString();
                    oDS.ReadXml(new System.IO.StringReader(sXML));
                }
            }
            catch
            {
            }

            return oDS;
        }
		#endregion

		#region tblDevices
		public Points.tbl_Devices getData(string sURI, string sAppToken, string sUserToken, Points.tbl_Devices dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_Devices>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tblPointUniqueValues
        public Points.tbl_PointUniqueValues getData(string sURI, string sAppToken, string sUserToken, Points.tbl_PointUniqueValues dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_PointUniqueValues>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tblPoints
        public Points.tbl_Points getData(string sURI, string sAppToken, string sUserToken, Points.tbl_Points dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_Points>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tblPointRanges
        public Points.tbl_PointRanges getData(string sURI, string sAppToken, string sUserToken, Points.tbl_PointRanges dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_PointRanges>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tblDeviceUniqueValues
        public Points.tbl_DeviceUniqueValues getData(string sURI, string sAppToken, string sUserToken, Points.tbl_DeviceUniqueValues dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_DeviceUniqueValues>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tblDAQs
        public Points.tbl_DAQs getData(string sURI, string sAppToken, string sUserToken, Points.tbl_DAQs dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_DAQs>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tblBuses
        public Points.tbl_Buses getData(string sURI, string sAppToken, string sUserToken, Points.tbl_Buses dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_Buses>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refVersions
        public Points.ref_Versions getData(string sURI, string sAppToken, string sUserToken, Points.ref_Versions dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_Versions>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refUnits
        public Points.ref_Units getData(string sURI, string sAppToken, string sUserToken, Points.ref_Units dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_Units>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refTranslationValues
        public Points.ref_TranslationValues getData(string sURI, string sAppToken, string sUserToken, Points.ref_TranslationValues dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_TranslationValues>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refTranslationTypes
        public Points.ref_TranslationTypes getData(string sURI, string sAppToken, string sUserToken, Points.ref_TranslationTypes dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_TranslationTypes>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refRecorders
        public Points.ref_Recorders getData(string sURI, string sAppToken, string sUserToken, Points.ref_Recorders dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_Recorders>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refRangeTypes
        public Points.ref_RangeTypes getData(string sURI, string sAppToken, string sUserToken, Points.ref_RangeTypes dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_RangeTypes>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refRangePresets
        public Points.ref_RangePresets getData(string sURI, string sAppToken, string sUserToken, Points.ref_RangePresets dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_RangePresets>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refPointUDFs
        public Points.ref_PointUDFs getData(string sURI, string sAppToken, string sUserToken, Points.ref_PointUDFs dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_PointUDFs>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region refDriverTypes
        public Points.ref_DriverTypes getData(string sURI, string sAppToken, string sUserToken, Points.ref_DriverTypes dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_DriverTypes>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region ref_DeviceUDFs
        public Points.ref_DeviceUDFs getData(string sURI, string sAppToken, string sUserToken, Points.ref_DeviceUDFs dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_DeviceUDFs>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region ref_DeviceTypes
        public Points.ref_DeviceTypes getData(string sURI, string sAppToken, string sUserToken, Points.ref_DeviceTypes dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_DeviceTypes>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region ref_BusTypes
        public Points.ref_BusTypes getData(string sURI, string sAppToken, string sUserToken, Points.ref_BusTypes dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_BusTypes>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region lst_RecorderPoints
        public Points.lst_RecorderPoints getData(string sURI, string sAppToken, string sUserToken, Points.lst_RecorderPoints dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.lst_RecorderPoints>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tbl_HistoricPointUniqueValues
        public Points.tbl_HistoricPointUniqueValues getData(string sURI, string sAppToken, string sUserToken, Points.tbl_HistoricPointUniqueValues dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_HistoricPointUniqueValues>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tbl_HistoricPointDatas
        public Points.tbl_HistoricPointDatas getData(string sURI, string sAppToken, string sUserToken, Points.tbl_HistoricPointDatas dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_HistoricPointDatas>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tbl_CalibrationRows
        public Points.tbl_CalibrationRows getData(string sURI, string sAppToken, string sUserToken, Points.tbl_CalibrationRows dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_CalibrationRows>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region tbl_CalibrationLists
        public Points.tbl_CalibrationLists getData(string sURI, string sAppToken, string sUserToken, Points.tbl_CalibrationLists dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.tbl_CalibrationLists>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion

        #region ref_CalibrationValuesTypes
        public Points.ref_CalibrationValuesTypes getData(string sURI, string sAppToken, string sUserToken, Points.ref_CalibrationValuesTypes dataRecs)
        {
            var oWebRequest = (HttpWebRequest)WebRequest.Create(sURI);
            oWebRequest.Method = "GET";
            //oWebRequest.Headers.Add(clsStaticShared.sAppHeader.ToString(), sAppToken);
            //oWebRequest.Headers.Add("token", sUserToken);
            oWebRequest.ContentLength = 0;
            oWebRequest.Timeout = 1000000;

            try
            {
                var oWebResponse = (HttpWebResponse)oWebRequest.GetResponse();

                if (oWebResponse.StatusCode == HttpStatusCode.OK)
                {
                    var oReader = new StreamReader(oWebResponse.GetResponseStream());
                    string sData = oReader.ReadToEnd();

                    dataRecs = Newtonsoft.Json.JsonConvert.DeserializeObject<Points.ref_CalibrationValuesTypes>(sData);
                }
            }
            catch (Exception ex)
            {
            }

            return dataRecs;
        }
        #endregion
    }
}
