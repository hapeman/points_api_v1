﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace PointsAPI_V1.Models
{
	public class dbFunctions
	{
		//dave server
		private static string IPAddress = "72.23.15.113";
		private static string DBName = "ShopTesting";
		//local machine
		//private static string IPAddress = @"BLACKMAC\SQLEXPRESS";

		//dave server 2
		//private static string IPAddress = "HSS-110";

		//building 50
		//private static string IPAddress = "10.1.1.10";

		//string CONNECTION_STRING = "Data Source=" + IPAddress + ";Initial Catalog=@@initialCatalog@@;User ID=sa;Password=ncc0817A;"; // HSS110 - Production (Phoenix)

		//NeilTest (local machine)
		//string CONNECTION_STRING = "Data Source=" + IPAddress + ";Initial Catalog=NeilTest;persist security info=True;Integrated Security=SSPI;"; // HSS110 - Production (Phoenix)

		//NeilTest (Dave's server)
		//string CONNECTION_STRING = "Data Source=" + IPAddress + ";Initial Catalog=NeilTest;User ID=sa;Password=ncc0817A;"; // HSS110 - Production (Phoenix)

		//NewDefault
		string CONNECTION_STRING = "Data Source=" + IPAddress + ";Initial Catalog=" + DBName + ";User ID=sa;Password=ncc0817A;"; // HSS110 - Production (Phoenix)

		//NeilTest (bld50 server)
		//string CONNECTION_STRING = "Data Source=" + IPAddress + ";Initial Catalog=NeilTest;persist security info=True;User ID=sa;Password=hap3man;";

		//Humidity (bld50 server)
		//string CONNECTION_STRING = "Data Source=" + IPAddress + ";Initial Catalog=Humidity;persist security info=True;User ID=sa;Password=hap3man;";

		#region Simple Get
		public DataSet getData(string storedProcedure, string initialCatalog)
        {
            return getData(storedProcedure, initialCatalog, new Dictionary<string, object>());
        }
        public DataSet getData(string storedProcedure, string initialCatalog, string sKeyName, string sKeyValue)
        {
            return getData(storedProcedure, initialCatalog, new Dictionary<string, object>
            {
                { sKeyName, sKeyValue }
            });
        }
        public DataSet getData(string storedProcedure, string initialCatalog, Dictionary<string, object> parameters)
        {
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection();
            if (initialCatalog != "")
            {
                conn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", initialCatalog);
            }
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand(storedProcedure, conn);
            System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter();
            adapter.SelectCommand = cmd;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandTimeout = 0;
            foreach (KeyValuePair<string, object> entry in parameters)
            {
                if (!string.IsNullOrEmpty(entry.Key) && entry.Value != null)
                {
                    adapter.SelectCommand.Parameters.AddWithValue(entry.Key, entry.Value);
                }
            }
            DataSet dataSet = new DataSet();
            try
            {
                adapter.Fill(dataSet);
            }
            catch (Exception ex)
            {
                dataSet = null;
            }
            return dataSet;
        }
		#endregion

		#region INSERT/UPDATE/DELETE

		#region tblHistoricPointValues
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_HistoricPointValue data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHPoint_ID", data.keyHPoint_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHPoint_realCurrentValue", data.HPoint_realCurrentValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHPoint_realDisplayValue", data.HPoint_realDisplayValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHPoint_realRawValue", data.HPoint_realRawValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHPoint_datetime", data.HPoint_datetime.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHPoint_ID", data.keyHPoint_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHPoint_realCurrentValue", data.HPoint_realCurrentValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHPoint_realDisplayValue", data.HPoint_realDisplayValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHPoint_realRawValue", data.HPoint_realRawValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHPoint_datetime", data.HPoint_datetime.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHPoint_ID", data.keyHPoint_ID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_HistoricPointValues datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_HistoricPointValues";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();

			foreach (HE_Models.Points.tbl_HistoricPointValue data in datas)//datas is being passed into processSP as null 
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblDevices
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_Device data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDeviceID", data.keyDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDAQ_ID", data.fkDAQ_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_strName", data.Dev_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDevTypeID", data.fkDevTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_boolTreeExpanded", data.Dev_boolTreeExpanded.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_boolEnabled", data.Dev_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_intLoadOrder", data.Dev_intLoadOrder.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkBusID", data.fkBusID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_boolSimulated", data.Dev_boolSimulated.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDeviceID", data.keyDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDAQ_ID", data.fkDAQ_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_strName", data.Dev_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDevTypeID", data.fkDevTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_boolTreeExpanded", data.Dev_boolTreeExpanded.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_boolEnabled", data.Dev_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_intLoadOrder", data.Dev_intLoadOrder.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkBusID", data.fkBusID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDev_boolSimulated", data.Dev_boolSimulated.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDeviceID", data.keyDeviceID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_Devices datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_Devices";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();

			foreach (HE_Models.Points.tbl_Device data in datas)//datas is being passed into processSP as null 
			{ 
					storedprocParameters(sAction, oDA, data);
					try
					{
						oCmd.ExecuteNonQuery();
					}
					catch (Exception sException)
					{
						sErrorMsg += sException.ToString();
					}
			}
			oConn.Close();

			return sErrorMsg;
		}
		
		#endregion

		#region tblPointUniqueValues
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_PointUniqueValue data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointUV_ID", data.keyPointUV_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPUDF_ID", data.fkPUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointUV_strValue", data.PointUV_strValue.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointUV_ID", data.keyPointUV_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPUDF_ID", data.fkPUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointUV_strValue", data.PointUV_strValue.ToString());
					break;
				case "UPDATEVALUE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inKeyPointUV_ID", data.keyPointUV_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointUV_strValue", data.PointUV_strValue.ToString());
					break;
				case "UPDATEVALUE_FORFOREIGNKEYS":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPUDF_ID", data.fkPUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointUV_strValue", data.PointUV_strValue.ToString());
					break;
				case "RESETDEFAULT_FORPOINT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPointID", data.fkPointID.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDeviceID", data.keyPointUV_ID.ToString());
					break;
				case "DELETEFIELD":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPUDF_ID", data.fkPUDF_ID.ToString());
					break;
				case "DELETEPOINT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPointID", data.fkPointID.ToString());
					break;
				case "ADDFIELD":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inKeyPUDF_ID", data.fkPUDF_ID.ToString());
					break;
				case "ADDPOINT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDevTypeID", data.fkPUDF_ID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_PointUniqueValues datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_PointUniqueValues";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_PointUniqueValue data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblPoints
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_Point data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolEnabled", data.Point_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intIndex", data.Point_intIndex.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitsID", data.fkUnitsID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strName", data.Point_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDescription", data.Point_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intDisplayDecimals", data.Point_intDisplayDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intRecordDecimals", data.Point_intRecordDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strFormulaText", data.Point_strFormulaText.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolControlPoint", data.Point_boolControlPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolIsBoolean", data.Point_boolIsBoolean.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolTrackStats", data.Point_boolTrackStats.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolArrayPoint", data.Point_boolArrayPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDisplayName", data.Point_strDisplayName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strNotes", data.Point_strNotes.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strTag", data.Point_strTag.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intCalInterval", data.Point_intCalInterval.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareSlope", data.Point_realHardwareSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareOffset", data.Point_realHardwareOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerSlope", data.Point_realTransducerSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerOffset", data.Point_realTransducerOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realRawValue", data.Point_realRawValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realCurrentValue", data.Point_realCurrentValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realDisplayValue", data.Point_realDisplayValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_dateUpdateTime", data.Point_dateUpdateTime.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_singleFutureValue", data.Point_singleFutureValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolHasFutureValue", data.Point_boolHasFutureValue.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolEnabled", data.Point_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intIndex", data.Point_intIndex.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitsID", data.fkUnitsID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strName", data.Point_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDescription", data.Point_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intDisplayDecimals", data.Point_intDisplayDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intRecordDecimals", data.Point_intRecordDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strFormulaText", data.Point_strFormulaText.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolControlPoint", data.Point_boolControlPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolIsBoolean", data.Point_boolIsBoolean.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolTrackStats", data.Point_boolTrackStats.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolArrayPoint", data.Point_boolArrayPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDisplayName", data.Point_strDisplayName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strNotes", data.Point_strNotes.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strTag", data.Point_strTag.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intCalInterval", data.Point_intCalInterval.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareSlope", data.Point_realHardwareSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareOffset", data.Point_realHardwareOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerSlope", data.Point_realTransducerSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerOffset", data.Point_realTransducerOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realRawValue", data.Point_realRawValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realCurrentValue", data.Point_realCurrentValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realDisplayValue", data.Point_realDisplayValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_dateUpdateTime", data.Point_dateUpdateTime.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_singleFutureValue", data.Point_singleFutureValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolHasFutureValue", data.Point_boolHasFutureValue.ToString());
					break;
				case "INSERTORUPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolEnabled", data.Point_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intIndex", data.Point_intIndex.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitsID", data.fkUnitsID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strName", data.Point_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDescription", data.Point_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intDisplayDecimals", data.Point_intDisplayDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intRecordDecimals", data.Point_intRecordDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strFormulaText", data.Point_strFormulaText.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolControlPoint", data.Point_boolControlPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolIsBoolean", data.Point_boolIsBoolean.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolTrackStats", data.Point_boolTrackStats.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolArrayPoint", data.Point_boolArrayPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDisplayName", data.Point_strDisplayName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strNotes", data.Point_strNotes.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strTag", data.Point_strTag.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intCalInterval", data.Point_intCalInterval.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareSlope", data.Point_realHardwareSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareOffset", data.Point_realHardwareOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerSlope", data.Point_realTransducerSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerOffset", data.Point_realTransducerOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realRawValue", data.Point_realRawValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realCurrentValue", data.Point_realCurrentValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realDisplayValue", data.Point_realDisplayValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_dateUpdateTime", data.Point_dateUpdateTime.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_singleFutureValue", data.Point_singleFutureValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolHasFutureValue", data.Point_boolHasFutureValue.ToString());
					break;
				case "UPDATECURRENTVALUE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realCurrentValue", data.Point_realCurrentValue.ToString());
					break;
				case "UPDATEALLVALUES":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realCurrentValue", data.Point_realCurrentValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realRawValue", data.Point_realRawValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realDisplayValue", data.Point_realDisplayValue.ToString());
					break;
				case "UPDATECURRENTVALUE_FROMBODY":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolEnabled", data.Point_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intIndex", data.Point_intIndex.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitsID", data.fkUnitsID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strName", data.Point_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDescription", data.Point_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intDisplayDecimals", data.Point_intDisplayDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intRecordDecimals", data.Point_intRecordDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strFormulaText", data.Point_strFormulaText.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolControlPoint", data.Point_boolControlPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolIsBoolean", data.Point_boolIsBoolean.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolTrackStats", data.Point_boolTrackStats.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolArrayPoint", data.Point_boolArrayPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDisplayName", data.Point_strDisplayName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strNotes", data.Point_strNotes.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strTag", data.Point_strTag.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intCalInterval", data.Point_intCalInterval.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareSlope", data.Point_realHardwareSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareOffset", data.Point_realHardwareOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerSlope", data.Point_realTransducerSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerOffset", data.Point_realTransducerOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realRawValue", data.Point_realRawValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realCurrentValue", data.Point_realCurrentValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realDisplayValue", data.Point_realDisplayValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_dateUpdateTime", data.Point_dateUpdateTime.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_singleFutureValue", data.Point_singleFutureValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolHasFutureValue", data.Point_boolHasFutureValue.ToString());
					break;
				case "UPDATEALLVALUES_FROMBODY":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolEnabled", data.Point_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intIndex", data.Point_intIndex.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitsID", data.fkUnitsID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strName", data.Point_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDescription", data.Point_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intDisplayDecimals", data.Point_intDisplayDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intRecordDecimals", data.Point_intRecordDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strFormulaText", data.Point_strFormulaText.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolControlPoint", data.Point_boolControlPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolIsBoolean", data.Point_boolIsBoolean.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolTrackStats", data.Point_boolTrackStats.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolArrayPoint", data.Point_boolArrayPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strDisplayName", data.Point_strDisplayName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strNotes", data.Point_strNotes.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_strTag", data.Point_strTag.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_intCalInterval", data.Point_intCalInterval.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareSlope", data.Point_realHardwareSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realHardwareOffset", data.Point_realHardwareOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerSlope", data.Point_realTransducerSlope.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realTransducerOffset", data.Point_realTransducerOffset.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realRawValue", data.Point_realRawValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realCurrentValue", data.Point_realCurrentValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_realDisplayValue", data.Point_realDisplayValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_dateUpdateTime", data.Point_dateUpdateTime.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_singleFutureValue", data.Point_singleFutureValue.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_boolHasFutureValue", data.Point_boolHasFutureValue.ToString());
					break;
				case "SETFUTUREVALUEANDPENDING":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPoint_singleFutureValue", data.Point_singleFutureValue.ToString());
					break;
				case "CLEARPENDING":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointID", data.keyPointID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_Points datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_Points";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_Point data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblPointRanges
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_PointRange data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointRangeID", data.keyPointRangeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkRangeTypeID", data.fkRangeTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointRange_fltMin", data.PointRange_fltMin.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointRange_fltMax", data.PointRange_fltMax.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointRange_boolDisabled", data.PointRange_boolDisabled.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointRangeID", data.keyPointRangeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkRangeTypeID", data.fkRangeTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointRange_fltMin", data.PointRange_fltMin.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointRange_fltMax", data.PointRange_fltMax.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPointRange_boolDisabled", data.PointRange_boolDisabled.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPointRangeID", data.keyPointRangeID.ToString());
					break;
				case "DELETERANGES_FORPOINT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPointID", data.fkPointID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_PointRanges datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_PointRanges";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_PointRange data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblDeviceUniqueValues
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_DeviceUniqueValue data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDevUV_ID", data.keyDevUV_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDUDF_ID", data.fkDUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevUV_strValue", data.DevUV_strValue.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDevUV_ID", data.keyDevUV_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDUDF_ID", data.fkDUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevUV_strValue", data.DevUV_strValue.ToString());
					break;
				case "UPDATEVALUE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inKeyDevUV_ID", data.keyDevUV_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevUV_strValue", data.DevUV_strValue.ToString());
					break;
				case "UPDATEVALUE_FORFOREIGNKEYS":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDUDF_ID", data.fkDUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevUV_strValue", data.DevUV_strValue.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDevUV_ID", data.keyDevUV_ID.ToString());
					break;
				case "DELETEDEVICE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDeviceID", data.fkDeviceID.ToString());
					break;
				case "DELETEFIELD":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDUDF_ID", data.fkDUDF_ID.ToString());
					break;
				case "ADDDEVICE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDevTypeID", data.fkDUDF_ID.ToString());
					break;
				case "ADDFIELD":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inKeyDUDF_ID", data.fkDUDF_ID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_DeviceUniqueValues datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_DeviceUniqueValues";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_DeviceUniqueValue data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblDAQs
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_DAQ data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDAQ_ID", data.keyDAQ_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDAQ_strName", data.DAQ_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDAQ_strLocation", data.DAQ_strLocation.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDAQ_boolTreeExpanded", data.DAQ_boolTreeExpanded.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDAQ_boolEnabled", data.DAQ_boolEnabled.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDAQ_ID", data.keyDAQ_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDAQ_strName", data.DAQ_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDAQ_strLocation", data.DAQ_strLocation.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDAQ_boolTreeExpanded", data.DAQ_boolTreeExpanded.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDAQ_boolEnabled", data.DAQ_boolEnabled.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDAQ_ID", data.keyDAQ_ID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_DAQs datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_DAQ";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_DAQ data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblBuses
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_Bus data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyBusID", data.keyBusID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inBus_strName", data.Bus_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDAQ_ID", data.fkDAQ_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkBusTypeID", data.fkBusTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inBus_strNotes", data.Bus_strNotes.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@keyBusID", data.keyBusID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@Bus_strName", data.Bus_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@fkDAQ_ID", data.fkDAQ_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@fkBusTypeID", data.fkBusTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@Bus_strNotes", data.Bus_strNotes.ToString()); 
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyBusID", data.keyBusID.ToString());
					break;
				case "DELETEALL_FORDAQ":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inKeyDAQ_ID", data.fkDAQ_ID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_Buses datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_Buses";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_Bus data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refVersions
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_Version data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyVersionID", data.keyVersionID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inVersion_strIntelimationVersion", data.Version_strIntelimationVersion.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inVersion_sdtInstallDate", data.Version_sdtInstallDate.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inVersion_strNotes", data.Version_strNotes.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyVersionID", data.keyVersionID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inVersion_strIntelimationVersion", data.Version_strIntelimationVersion.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inVersion_sdtInstallDate", data.Version_sdtInstallDate.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inVersion_strNotes", data.Version_strNotes.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyVersionID", data.keyVersionID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_Versions datas)
		{
			string sErrorMsg = ""; string tblName = "ref_Version";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_Version data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refUnits
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_Unit data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyUnitID", data.keyUnitID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inUnit_strUnits", data.Unit_strUnits.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inUnit_boolTreeExpanded", data.Unit_boolTreeExpanded.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyUnitID", data.keyUnitID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inUnit_strUnits", data.Unit_strUnits.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inUnit_boolTreeExpanded", data.Unit_boolTreeExpanded.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyUnitID", data.keyUnitID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_Units datas)
		{
			string sErrorMsg = ""; string tblName = "ref_Units";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_Unit data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refTranslationValues
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_TranslationValue data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyTranslationValueID", data.keyTranslationValueID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inTranValue_Value", data.TranValue_Value.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inTranValue_Translation", data.TranValue_Translation.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyTranslationValueID", data.keyTranslationValueID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inTranValue_Value", data.TranValue_Value.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inTranValue_Translation", data.TranValue_Translation.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyTranslationValueID", data.keyTranslationValueID.ToString());
					break;
				case "DELETEFORTRANSLATIONTYPE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inKeyTranslationTypeID", data.fkTranslationTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_TranslationValues datas)
		{
			string sErrorMsg = ""; string tblName = "ref_TranslationValues";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_TranslationValue data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refTranslationTypes
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_TranslationType data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyTranslationTypeID", data.keyTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inTransType_Name", data.TransType_Name.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inTransType_Description", data.TransType_Description.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyTranslationTypeID", data.keyTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inTransType_Name", data.TransType_Name.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inTransType_Description", data.TransType_Description.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyTranslationTypeID", data.keyTranslationTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_TranslationTypes datas)
		{
			string sErrorMsg = ""; string tblName = "ref_TranslationTypes";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_TranslationType data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refRecorders
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_Recorder data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRecorderID", data.keyRecorderID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRecorder_strName", data.Recorder_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRecorder_intBit", data.Recorder_intBit.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRecorderID", data.keyRecorderID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRecorder_strName", data.Recorder_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRecorder_intBit", data.Recorder_intBit.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRecorderID", data.keyRecorderID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_Recorders datas)
		{
			string sErrorMsg = ""; string tblName = "ref_Recorders";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_Recorder data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refRangeTypes
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_RangeType data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRangeTypeID", data.keyRangeTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_intOrder", data.RangeType_intOrder.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strName", data.RangeType_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strBelowMinColor", data.RangeType_strBelowMinColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strInRangeColor", data.RangeType_strInRangeColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strOverMaxColor", data.RangeType_strOverMaxColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_boolTreeExpanded", data.RangeType_boolTreeExpanded.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_boolShutDownRange", data.RangeType_boolShutDownRange.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_boolSensorLimitRange", data.RangeType_boolSensorLimitRange.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_boolDisabled", data.RangeType_boolDisabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strInRangeForeColor", data.RangeType_strInRangeForeColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strInRangeBackColor", data.RangeType_strInRangeBackColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strUnderRangeForeColor", data.RangeType_strUnderRangeForeColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strUnderRangeBackColor", data.RangeType_strUnderRangeBackColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strOverRangeForeColor", data.RangeType_strOverRangeForeColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strOverRangeBackColor", data.RangeType_strOverRangeBackColor.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRangeTypeID", data.keyRangeTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_intOrder", data.RangeType_intOrder.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strName", data.RangeType_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strBelowMinColor", data.RangeType_strBelowMinColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strInRangeColor", data.RangeType_strInRangeColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strOverMaxColor", data.RangeType_strOverMaxColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_boolTreeExpanded", data.RangeType_boolTreeExpanded.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_boolShutDownRange", data.RangeType_boolShutDownRange.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_boolSensorLimitRange", data.RangeType_boolSensorLimitRange.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_boolDisabled", data.RangeType_boolDisabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strInRangeForeColor", data.RangeType_strInRangeForeColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strInRangeBackColor", data.RangeType_strInRangeBackColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strUnderRangeForeColor", data.RangeType_strUnderRangeForeColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strUnderRangeBackColor", data.RangeType_strUnderRangeBackColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strOverRangeForeColor", data.RangeType_strOverRangeForeColor.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangeType_strOverRangeBackColor", data.RangeType_strOverRangeBackColor.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRangeTypeID", data.keyRangeTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_RangeTypes datas)
		{
			string sErrorMsg = ""; string tblName = "ref_RangeTypes";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_RangeType data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refRangePresets
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_RangePreset data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRangePresetID", data.keyRangePresetID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkRangeTypeID", data.fkRangeTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitID", data.fkUnitID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_strName", data.RangePreset_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_fltMin", data.RangePreset_fltMin.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_fltMax", data.RangePreset_fltMax.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_boolDefault", data.RangePreset_boolDefault.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_boolTreeExpanded", data.RangePreset_boolTreeExpanded.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRangePresetID", data.keyRangePresetID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkRangeTypeID", data.fkRangeTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitID", data.fkUnitID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_strName", data.RangePreset_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_fltMin", data.RangePreset_fltMin.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_fltMax", data.RangePreset_fltMax.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_boolDefault", data.RangePreset_boolDefault.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inRangePreset_boolTreeExpanded", data.RangePreset_boolTreeExpanded.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRangePresetID", data.keyRangePresetID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_RangePresets datas)
		{
			string sErrorMsg = ""; string tblName = "ref_RangePresets";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_RangePreset data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refPointUDFs
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_PointUDF data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPUDF_ID", data.keyPUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDevTypeID", data.fkDevTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_strFieldName", data.PUDF_strFieldName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_strDescription", data.PUDF_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_strDefault", data.PUDF_strDefault.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_boolNonDeveloper", data.PUDF_boolNonDeveloper.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_strFieldDef", data.PUDF_strFieldDef.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPUDF_ID", data.keyPUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDevTypeID", data.fkDevTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_strFieldName", data.PUDF_strFieldName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_strDescription", data.PUDF_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_strDefault", data.PUDF_strDefault.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_boolNonDeveloper", data.PUDF_boolNonDeveloper.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inPUDF_strFieldDef", data.PUDF_strFieldDef.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPUDF_ID", data.keyPUDF_ID.ToString());
					break;
				case "DELETEFIELDS_FORDEVICETYPE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDevTypeID", data.fkDevTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_PointUDFs datas)
		{
			string sErrorMsg = ""; string tblName = "ref_PointUDFs";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_PointUDF data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refDriverTypes
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_DriverType data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPUDF_ID", data.keyDriverTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDevTypeID", data.DriverTypestrValue.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyPUDF_ID", data.keyDriverTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDevTypeID", data.DriverTypestrValue.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDriverTypeID", data.keyDriverTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_DriverTypes datas)
		{
			string sErrorMsg = ""; string tblName = "ref_DriverTypes";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_DriverType data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refDeviceUDFs
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_DeviceUDF data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDUDF_ID", data.keyDUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDevTypeID", data.fkDevTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_strFieldName", data.DUDF_strFieldName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_strDescription", data.DUDF_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_strDefault", data.DUDF_strDefault.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_boolNonDeveloper", data.DUDF_boolNonDeveloper.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_strFieldDef", data.DUDF_strFieldDef.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDUDF_ID", data.keyDUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDevTypeID", data.fkDevTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_strFieldName", data.DUDF_strFieldName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_strDescription", data.DUDF_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_strDefault", data.DUDF_strDefault.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_boolNonDeveloper", data.DUDF_boolNonDeveloper.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDUDF_strFieldDef", data.DUDF_strFieldDef.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDUDF_ID", data.keyDUDF_ID.ToString());
					break;
				case "DELETEFIELDS_FORDEVICETYPE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkDevTypeID", data.fkDevTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_DeviceUDFs datas)
		{
			string sErrorMsg = ""; string tblName = "ref_DeviceUDFs";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_DeviceUDF data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refDeviceTypes
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_DeviceType data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDevTypeID", data.keyDevTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevType_strName", data.DevType_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDriverTypeID", data.fkDriverTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevType_intLoadOrder", data.DevType_intLoadOrder.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevType_strEl_ClassName", data.DevType_strEl_ClassName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevType_strWIZ_ClassName", data.DevType_strWIZ_ClassName.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDevTypeID", data.keyDevTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevType_strName", data.DevType_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDriverTypeID", data.fkDriverTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevType_intLoadOrder", data.DevType_intLoadOrder.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevType_strEl_ClassName", data.DevType_strEl_ClassName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inDevType_strWIZ_ClassName", data.DevType_strWIZ_ClassName.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyDevTypeID", data.keyDevTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_DeviceTypes datas)
		{
			string sErrorMsg = ""; string tblName = "ref_DeviceTypes";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_DeviceType data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refBusTypes
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_BusType data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyBusTypeID", data.keyBusTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inBusType_strName", data.BusType_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inBusType_strDescription", data.BusType_strDescription.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyBusTypeID", data.keyBusTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inBusType_strName", data.BusType_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inBusType_strDescription", data.BusType_strDescription.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyBusTypeID", data.keyBusTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_BusTypes datas)
		{
			string sErrorMsg = ""; string tblName = "ref_BusTypes";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_BusType data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region lstRecorderPoints
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.lst_RecorderPoint data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRecorderPointID", data.keyRecorderPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkRecorderID", data.fkRecorderID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					break;
				case "INSERTRECORDERPOINT":
					//dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRecorderPointID", Guid.NewGuid().ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkRecorderID", data.fkRecorderID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRecorderPointID", data.keyRecorderPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkRecorderID", data.fkRecorderID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyRecorderPointID", data.keyRecorderPointID.ToString());
					break;
				case "DELETEPOINTS_FORRECORDER":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkRecorderID", data.fkRecorderID.ToString());
					break;
				case "DELETERECORDERPOINT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkRecorderID", data.fkRecorderID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPointID", data.fkPointID.ToString());
					break;
				case "DELETERECORDERS_FORPOINT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inFkPointID", data.fkPointID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.lst_RecorderPoints datas)
		{
			string sErrorMsg = ""; string tblName = "lst_RecorderPoints";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.lst_RecorderPoint data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblHistoricPointUniqueValues
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_HistoricPointUniqueValue data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHistoricPointUniqueValueID", data.keyHistoricPointUniqueValueID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkHistroicPointDataID", data.fkHistroicPointDataID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPUDF_ID", data.fkPUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointUniqueValue_strValue", data.HistoricPointUniqueValue_strValue.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHistoricPointUniqueValueID", data.keyHistoricPointUniqueValueID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkHistroicPointDataID", data.fkHistroicPointDataID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPUDF_ID", data.fkPUDF_ID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointUniqueValue_strValue", data.HistoricPointUniqueValue_strValue.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHistoricPointUniqueValueID", data.keyHistoricPointUniqueValueID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_HistoricPointUniqueValues datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_HistoricPointUniqueValues";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_HistoricPointUniqueValue data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblHistoricPointDatas
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_HistoricPointData data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHisoricPointData", data.keyHisoricPointData.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkCalibrationListID", data.fkCalibrationListID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@ininPoint_HistoricPointData_dateDateRetiredintIndex", data.HistoricPointData_dateDateRetired.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@ininHistoricPointData_boolEnabled", data.HistoricPointData_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_intIndex", data.HistoricPointData_intIndex.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitsID", data.fkUnitsID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strName", data.HistoricPointData_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strDescription", data.HistoricPointData_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_intDisplayDecimals", data.HistoricPointData_intDisplayDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_intRecordDecimals", data.HistoricPointData_intRecordDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strFormulaText", data.HistoricPointData_strFormulaText.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@ininHistoricPointData_boolControlPoint", data.HistoricPointData_boolControlPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_boolIsBoolean", data.HistoricPointData_boolIsBoolean.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_boolTrackStats", data.HistoricPointData_boolTrackStats.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_boolArrayPoint", data.HistoricPointData_boolArrayPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strDisplayName", data.HistoricPointData_strDisplayName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strNotes", data.HistoricPointData_strNotes.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strTag", data.HistoricPointData_strTag.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_intCalInterval", data.HistoricPointData_intCalInterval.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHisoricPointData", data.keyHisoricPointData.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkCalibrationListID", data.fkCalibrationListID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@ininPoint_HistoricPointData_dateDateRetiredintIndex", data.HistoricPointData_dateDateRetired.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDeviceID", data.fkDeviceID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@ininHistoricPointData_boolEnabled", data.HistoricPointData_boolEnabled.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_intIndex", data.HistoricPointData_intIndex.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkUnitsID", data.fkUnitsID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strName", data.HistoricPointData_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strDescription", data.HistoricPointData_strDescription.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkTranslationTypeID", data.fkTranslationTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_intDisplayDecimals", data.HistoricPointData_intDisplayDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_intRecordDecimals", data.HistoricPointData_intRecordDecimals.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strFormulaText", data.HistoricPointData_strFormulaText.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@ininHistoricPointData_boolControlPoint", data.HistoricPointData_boolControlPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_boolIsBoolean", data.HistoricPointData_boolIsBoolean.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_boolTrackStats", data.HistoricPointData_boolTrackStats.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_boolArrayPoint", data.HistoricPointData_boolArrayPoint.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strDisplayName", data.HistoricPointData_strDisplayName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strNotes", data.HistoricPointData_strNotes.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_strTag", data.HistoricPointData_strTag.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inHistoricPointData_intCalInterval", data.HistoricPointData_intCalInterval.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyHisoricPointData", data.keyHisoricPointData.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_HistoricPointDatas datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_HistoricPointDatas";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_HistoricPointData data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblCalibrationRows
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_CalibrationRow data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationRowID", data.keyCalibrationRowID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkCalibrationListID", data.fkCalibrationListID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDataSummaryGroupID", data.fkDataSummaryGroupID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkCalibrationValuesTypeID", data.fkCalibrationValuesTypeID.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationRowID", data.keyCalibrationRowID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkCalibrationListID", data.fkCalibrationListID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkDataSummaryGroupID", data.fkDataSummaryGroupID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkCalibrationValuesTypeID", data.fkCalibrationValuesTypeID.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationRowID", data.keyCalibrationRowID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_CalibrationRows datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_CalibrationRows";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_CalibrationRow data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region tblCalibrationLists
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.tbl_CalibrationList data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationListID", data.keyCalibrationListID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationList_dateDateCreated", data.CalibrationList_dateDateCreated.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationList_dateDateRetired", data.CalibrationList_dateDateRetired.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationList_Active", data.CalibrationList_Active.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationListID", data.keyCalibrationListID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@infkPointID", data.fkPointID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationList_dateDateCreated", data.CalibrationList_dateDateCreated.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationList_dateDateRetired", data.CalibrationList_dateDateRetired.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationList_Active", data.CalibrationList_Active.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationListID", data.keyCalibrationListID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.tbl_CalibrationLists datas)
		{
			string sErrorMsg = ""; string tblName = "tbl_CalibrationLists";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.tbl_CalibrationList data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#region refCalibrationValuesTypes
		private void storedprocParameters(string actionType, System.Data.SqlClient.SqlDataAdapter dataAdapter, HE_Models.Points.ref_CalibrationValuesType data)
		{
			dataAdapter.SelectCommand.Parameters.Clear();
			switch (actionType.ToUpper())
			{
				case "INSERT":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationValuesTypeID", data.keyCalibrationValuesTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationValuesTypes_strName", data.CalibrationValuesTypes_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationValuesTypes_strDescription", data.CalibrationValuesTypes_strDescription.ToString());
					break;
				case "UPDATE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationValuesTypeID", data.keyCalibrationValuesTypeID.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationValuesTypes_strName", data.CalibrationValuesTypes_strName.ToString());
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inCalibrationValuesTypes_strDescription", data.CalibrationValuesTypes_strDescription.ToString());
					break;
				case "DELETE":
					dataAdapter.SelectCommand.Parameters.AddWithValue("@inkeyCalibrationValuesTypeID", data.keyCalibrationValuesTypeID.ToString());
					break;
				default:
					break;
			}

		}
		public string processSP(string environment, string sAction, HE_Models.Points.ref_CalibrationValuesTypes datas)
		{
			string sErrorMsg = ""; string tblName = "ref_CalibrationValuesTypes";
			System.Data.SqlClient.SqlConnection oConn = new System.Data.SqlClient.SqlConnection();
			oConn.ConnectionString = CONNECTION_STRING.Replace("@@initialCatalog@@", environment);

			System.Data.SqlClient.SqlCommand oCmd = new System.Data.SqlClient.SqlCommand("qry_" + tblName + "_" + sAction, oConn);
			System.Data.SqlClient.SqlDataAdapter oDA = new System.Data.SqlClient.SqlDataAdapter();
			oDA.SelectCommand = oCmd;
			oCmd.CommandType = CommandType.StoredProcedure;
			oCmd.CommandTimeout = 0;

			oConn.Open();
			foreach (HE_Models.Points.ref_CalibrationValuesType data in datas)
			{
				storedprocParameters(sAction, oDA, data);
				try
				{
					oCmd.ExecuteNonQuery();
				}
				catch (Exception sException)
				{
					sErrorMsg += sException.ToString();
				}
			}
			oConn.Close();

			return sErrorMsg;
		}
		#endregion

		#endregion
	}
}