﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;

namespace PointsAPI_V1.Models
{
    public class PointsController : ApiController
    {
		Models.dbFunctions oDB = new Models.dbFunctions();
		HE_Models.Points clsFunc = new HE_Models.Points();

		//***********************************	GET  ***********************************	

		#region GET

		#region tbl_HistoricPointValues

		//qry_tbl_HistoricPointValues_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/tblHistoricPointValues/{inkeyPointID}")]
		public HttpResponseMessage tblHistoricPointValues(string inkeyPointID)
		{
			string tblName = "tbl_HistoricPointValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", "Points", "@inkeyPointID", inkeyPointID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_Devices_Get

		//qry_tbl_Devices_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblDevices")]
		public HttpResponseMessage tblDevices_Class(string environment)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_Devices dataRecs = new HE_Models.Points.tbl_Devices();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_SelectKey *****DNE******
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblDevices/{keyDeviceID}")]
		public HttpResponseMessage tblDevices_Class(string environment, string keyDeviceID)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDeviceID", keyDeviceID);
				HE_Models.Points.tbl_Devices dataRecs = new HE_Models.Points.tbl_Devices();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblDevices")]
		public HttpResponseMessage tblDevices(string environment)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_SelectKey *****DNE******
		[HttpGet]
		[Route("api/V1/{environment}/tblDevices/{keyDeviceID}")]
		public HttpResponseMessage tblDevices(string environment, string keyDeviceID)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDeviceID", keyDeviceID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_ForDeviceType_ForDAQ
		[HttpGet]
		[Route("api/V1/{environment}/tblDevices/ForDevTypeAndDAQ/{in_DAQ_ID}/{in_DevTypeID}")]
		public HttpResponseMessage tblDevices_ForDevTypeAndDAQ(string environment, string in_DAQ_ID, string in_DevTypeID)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				Dictionary<string, object> myDict = new Dictionary<string, object> { {"@in_DAQ_ID", in_DAQ_ID }, {"@in_DevTypeID", in_DevTypeID } };
				oDS = oDB.getData("qry_" + tblName + "_ForDeviceType_ForDAQ", environment, myDict);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_ForDriver_ForDAQ
		[HttpGet]
		[Route("api/V1/{environment}/tblDevices/ForDriverAndDAQ/{in_DAQ_ID}/{in_DriverID}")]
		public HttpResponseMessage tblDevices_ForDriverAndDAQ(string environment, string in_DAQ_ID, string in_DriverID)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				Dictionary<string, object> myDict = new Dictionary<string, object> { { "@in_DAQ_ID", in_DAQ_ID }, { "@in_DriverID", in_DriverID } };
				oDS = oDB.getData("qry_" + tblName + "_ForDriver_ForDAQ", environment, myDict);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_GetParentInfo_ForDevice
		[HttpGet]
		[Route("api/V1/{environment}/tblDevices/Parent/{inKeyDeviceID}")]
		public HttpResponseMessage tblDevices_Parent(string environment, string inKeyDeviceID)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				oDS = oDB.getData("qry_" + tblName + "_GetParentInfo_ForDevice", environment, "@inKeyDeviceID", inKeyDeviceID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_MaxLoadOrder_ForDAQ
		[HttpGet]
		[Route("api/V1/{environment}/tblDevices/MaxLoadOrder/{inFkDAQ_ID}")]
		public HttpResponseMessage tblDevices_MaxLoadOrder(string environment, string inFkDAQ_ID)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				oDS = oDB.getData("qry_" + tblName + "_MaxLoadOrder_ForDAQ", environment, "@inFkDAQ_ID", inFkDAQ_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_SelectAll_ForDAQ
		[HttpGet]
		[Route("api/V1/{environment}/tblDevices/SelectAllForDAQ/{in_DAQ_ID}")]
		public HttpResponseMessage tblDevices_SelectAllForDAQ(string environment, string in_DAQ_ID)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				oDS = oDB.getData("qry_" + tblName + "_SelectAll_ForDAQ", environment, "@in_DAQ_ID", in_DAQ_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_SelectDevTypeID_ForDevice
		[HttpGet]
		[Route("api/V1/{environment}/tblDevices/SelectTypeForDevice/{inKeyDeviceID}")]
		public HttpResponseMessage tblDevices_SelectTypeForDevice(string environment, string inKeyDeviceID)
		{
			string tblName = "tbl_Devices";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				oDS = oDB.getData("qry_" + tblName + "_SelectDevTypeID_ForDevice", environment, "@inKeyDeviceID", inKeyDeviceID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}


		#endregion

		#region tbl_PointUniqueValues_Get

		//qry_tbl_PointUniqueValues_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblPointUniqueValues")]
		public HttpResponseMessage tblPointUniqueValues_Class(string environment)
		{
			string tblName = "tbl_PointUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointUniqueValues_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblPointUniqueValues/{keyPointUV_ID}")]
		public HttpResponseMessage tblPointUniqueValues_Class(string environment, string keyPointUV_ID)
		{
			string tblName = "tbl_PointUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyPointUV_ID", keyPointUV_ID);
				HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointUniqueValues_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblPointUniqueValues")]
		public HttpResponseMessage tblPointUniqueValues(string environment)
		{
			string tblName = "tbl_PointUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointUniqueValues_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/tblPointUniqueValues/{keyPointUV_ID}")]
		public HttpResponseMessage tblPointUniqueValues(string environment, string keyPointUV_ID)
		{
			string tblName = "tbl_PointUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyPointUV_ID", keyPointUV_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointUniqueValues_SelectAll_ForPointAndPUDF
		[HttpGet]
		[Route("api/V1/{environment}/tblPointUniqueValues/SelectForPointAndPUDF/{inKeyPointID}/{inKeyPUDFID}")]
		public HttpResponseMessage tblPointUniqueValues_ForPointAndPUDF(string environment, string inKeyPointID, string inKeyPUDFID)
		{
			string tblName = "tbl_PointUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				Dictionary<string, object> myDict = new Dictionary<string, object> { { "@inKeyPointID", inKeyPointID }, { "@inKeyPUDFID", inKeyPUDFID } };
				oDS = oDB.getData("qry_" + tblName + "_SelectAll_ForPointAndPUDF", environment, myDict);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region tbl_Points_Get

		//qry_tbl_Points_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblPoints")]
		public HttpResponseMessage tblPoints_Class(string environment)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblPoints/{keyPointID}")]
		public HttpResponseMessage tblPoints_Class(string environment, string keyPointID)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyPointID", keyPointID);
				HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblPoints")]
		public HttpResponseMessage tblPoints(string environment)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/tblPoints/{keyPointID}")]
		public HttpResponseMessage tblPoints(string environment, string keyPointID)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@inkeyPointID", keyPointID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_SelectPoints_ForDevice
		[HttpGet]
		[Route("api/V1/{environment}/tblPoints/SelectForDevice/{in_DeviceID}")]
		public HttpResponseMessage tblPoints_ForDevice(string environment, string in_DeviceID)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectPoints_ForDevice", environment, "@in_DeviceID", in_DeviceID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_GetParentInfo_ForPoint
		[HttpGet]
		[Route("api/V1/{environment}/tblPoints/GetParent/{inKeyPointID}")]
		public HttpResponseMessage tblPoints_ForDevice_Parent(string environment, string inKeyPointID)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_GetParentInfo_ForPoint", environment, "@inKeyPointID", inKeyPointID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_MaxIndex_ForDevice
		[HttpGet]
		[Route("api/V1/{environment}/tblPoints/MaxIndexForDevice/{inFkDeviceID}")]
		public HttpResponseMessage tblPoints_ForDevice_MaxIndexForDevice(string environment, string inFkDeviceID)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_MaxIndex_ForDevice", environment, "@inFkDeviceID", inFkDeviceID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_GetCurrentValue_ForPoint
		[HttpGet]
		[Route("api/V1/{environment}/tblPoints/GetCurrentValue/{keyPointID}")]
		public HttpResponseMessage tblPoints_CurrentValue(string environment, string keyPointID)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_GetCurrentValue_ForPoint", environment, "@inkeyPointID", keyPointID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_GetCurrentValue_DotName
		[HttpGet]
		[Route("api/V1/{environment}/tblPoints/GetCurrentValue/DotName/{dotName}")]
		public HttpResponseMessage tblPoints_CurrentValue_DotName(string environment, string dotName)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_GetCurrentValue_DotName", environment, "@inDotName", dotName);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_GetPendingPoints
		[HttpGet]
		[Route("api/V1/{environment}/tblPoints/GetPendingPoints")]
		public HttpResponseMessage tblPoints_GetPendingPoints(string environment)
		{
			string tblName = "tbl_Points";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_GetPendingPoints", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_PointRanges_Get

		//qry_tbl_PointRanges_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblPointRanges")]
		public HttpResponseMessage tblPointRangess_Class(string environment)
		{
			string tblName = "tbl_PointRanges";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_PointRanges dataRecs = new HE_Models.Points.tbl_PointRanges();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointRanges_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblPointRanges/{keyPointRangeID}")]
		public HttpResponseMessage tblPointRanges_Class(string environment, string keyPointRangeID)
		{
			string tblName = "tbl_PointRanges";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyPointRangeID", keyPointRangeID);
				HE_Models.Points.tbl_PointRanges dataRecs = new HE_Models.Points.tbl_PointRanges();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointRanges_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblPointRanges")]
		public HttpResponseMessage tblPointRanges(string environment)
		{
			string tblName = "tbl_PointRanges";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointRanges_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/tblPointRanges/{keyPointRangeID}")]
		public HttpResponseMessage tblPointRanges(string environment, string keyPointRangeID)
		{
			string tblName = "tbl_PointRanges";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyPointID", keyPointRangeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointRanges_SelectRange_ForPointAndType
		[HttpGet]
		[Route("api/V1/{environment}/tblPointRanges/SelectRangeForPointAndType/{inFkPointID}/{inFkRangeTypeID}")]
		public HttpResponseMessage tblPointRanges(string environment, string inFkPointID, string inFkRangeTypeID)
		{
			string tblName = "tbl_PointRanges";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				Dictionary<string, object> myDict = new Dictionary<string, object> { { "@inFkPointID", inFkPointID },{"@inFkRangeTypeID", inFkRangeTypeID } };
				oDS = oDB.getData("qry_" + tblName + "_SelectRange_ForPointAndType", environment, myDict);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointRanges_SelectRange_ForPoint
		[HttpGet]
		[Route("api/V1/{environment}/tblPointRanges/ForPoint/{fkPointID}")]
		public HttpResponseMessage tblPointRanges_ForPoint(string environment, string fkPointID)
		{
			string tblName = "tbl_PointRanges";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectRange_ForPoint", environment, "@inkeyPointID", fkPointID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region tbl_DeviceUniqueValues_Get

		//qry_tbl_DeviceUniqueValues_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblDeviceUniqueValues")]
		public HttpResponseMessage tblDeviceUniqueValues_Class(string environment)
		{
			string tblName = "tbl_DeviceUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DeviceUniqueValues_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblDeviceUniqueValues/{keyDevUD_ID}")]
		public HttpResponseMessage tblDeviceUniqueValues_Class(string environment, string keyDevUD_ID)
		{
			string tblName = "tbl_DeviceUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDevUD_ID", keyDevUD_ID);
				HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DeviceUniqueValues_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblDeviceUniqueValues")]
		public HttpResponseMessage tblDeviceUniqueValues(string environment)
		{
			string tblName = "tbl_DeviceUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DeviceUniqueValues_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/tblDeviceUniqueValues/{keyDevUD_ID}")]
		public HttpResponseMessage tblDeviceUniqueValues(string environment, string keyDevUD_ID)
		{
			string tblName = "tbl_DeviceUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDevUD_ID", keyDevUD_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DeviceUniqueValues_SelectAllForDeviceAndDUDF
		[HttpGet]
		[Route("api/V1/{environment}/tblDeviceUniqueValues/ForDeviceAndDUDF/{fkDeviceID}/{fkDUDF_ID}")]
		public HttpResponseMessage tblDeviceUniqueValues_ForDeviceAndDUDF(string environment, string fkDeviceID, string fkDUDF_ID)
		{
			string tblName = "tbl_DeviceUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				Dictionary<string, object> myDict = new Dictionary<string, object> { { "@infkDeviceID", fkDeviceID }, { "@infkDUDF_ID", fkDUDF_ID } };
				oDS = oDB.getData("qry_" + tblName + "_SelectAllForDeviceAndDUDF", environment, myDict);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		

		#endregion

		#region tbl_DAQ_Get

		//qry_tbl_DAQ_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblDAQ")]
		public HttpResponseMessage tblDAQ_Class(string environment)
		{
			string tblName = "tbl_DAQ";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_DAQs dataRecs = new HE_Models.Points.tbl_DAQs();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DAQ_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblDeviceUniqueValues/{keyDAQ_ID}")]
		public HttpResponseMessage tblDAQ_Class(string environment, string keyDAQ_ID)
		{
			string tblName = "tbl_DAQ";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDAQ_ID", keyDAQ_ID);
				HE_Models.Points.tbl_DAQs dataRecs = new HE_Models.Points.tbl_DAQs();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DAQ_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblDAQ")]
		public HttpResponseMessage tblDAQ(string environment)
		{
			string tblName = "tbl_DAQ";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DAQ_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/tblDAQ/{keyDAQ_ID}")]
		public HttpResponseMessage tblDAQ(string environment, string keyDAQ_ID)
		{
			string tblName = "tbl_DAQ";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDAQ_ID", keyDAQ_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_Buses_Get

		//qry_tbl_Buses_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblBuses")]
		public HttpResponseMessage tblBuses_Class(string environment)
		{
			string tblName = "tbl_Buses";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_Buses dataRecs = new HE_Models.Points.tbl_Buses();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Buses_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblBuses/{keyBusID}")]
		public HttpResponseMessage tblBuses_Class(string environment, string keyBusID)
		{
			string tblName = "tbl_Buses";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyBusID", keyBusID);
				HE_Models.Points.tbl_Buses dataRecs = new HE_Models.Points.tbl_Buses();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Buses_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblBuses")]
		public HttpResponseMessage tblBuses(string environment)
		{
			string tblName = "tbl_Buses";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Buses_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/tblBuses/{keyBusID}")]
		public HttpResponseMessage tblBuses(string environment, string keyBusID)
		{
			string tblName = "tbl_Buses";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@inkeyBusID", keyBusID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Buses_DevicesOnBus
		[HttpGet]
		[Route("api/V1/{environment}/tblBuses/DevicesOnBus/{inKeyBusID}")]
		public HttpResponseMessage tblBuses_Devices(string environment, string inKeyBusID)
		{
			string tblName = "tbl_Buses";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_DevicesOnBus", environment, "@inKeyBusID", inKeyBusID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Buses_SelectBuses_InDAQ
		[HttpGet]
		[Route("api/V1/{environment}/tblBuses/InDAQ/{inFkDAQ_ID}")]
		public HttpResponseMessage tblBuses_InDAQ(string environment, string inFkDAQ_ID)
		{
			string tblName = "tbl_Buses";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectBuses_InDAQ", environment, "@inFkDAQ_ID", inFkDAQ_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region ref_Versions_Get

		//qry's dne
		[HttpGet]
		[Route("api/V1/{environment}/Class/refVersions")]
		public HttpResponseMessage refVersions_Class(string environment)
		{
			string tblName = "ref_Version";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_Versions dataRecs = new HE_Models.Points.ref_Versions();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpGet]
		[Route("api/V1/{environment}/Class/refVersions/{keyVersionID}")]
		public HttpResponseMessage refVersions_Class(string environment, string keyVersionID)
		{
			string tblName = "ref_Version";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyVersionID", keyVersionID);
				HE_Models.Points.ref_Versions dataRecs = new HE_Models.Points.ref_Versions();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refVersions")]
		public HttpResponseMessage refVersions(string environment)
		{
			string tblName = "ref_Versions";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpGet]
		[Route("api/V1/{environment}/refVersions/{keyVersionID}")]
		public HttpResponseMessage refVersions(string environment, string keyVersionID)
		{
			string tblName = "ref_Version";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyVersionID", keyVersionID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_Units_Get

		//qry_ref_Units_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/refUnits")]
		public HttpResponseMessage refUnits_Class(string environment)
		{
			string tblName = "ref_Units";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_Units dataRecs = new HE_Models.Points.ref_Units();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_Units_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/refUnits/{keyUnitID}")]
		public HttpResponseMessage refUnits_Class(string environment, string keyUnitID)
		{
			string tblName = "ref_Units";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyUnitID", keyUnitID);
				HE_Models.Points.ref_Units dataRecs = new HE_Models.Points.ref_Units();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_Units_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/refUnits")]
		public HttpResponseMessage refUnits(string environment)
		{
			string tblName = "ref_Units";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_Units_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/refUnits/{keyUnitID}")]
		public HttpResponseMessage refUnits(string environment, string keyUnitID)
		{
			string tblName = "ref_Units";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyUnitID", keyUnitID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_Units_SelectPoints_UsingUnit
		[HttpGet]
		[Route("api/V1/{environment}/refUnits/SelectPoints/{inKeyUnitsID}")]
		public HttpResponseMessage refUnits_SelectPoints(string environment, string inKeyUnitsID)
		{
			string tblName = "ref_Units";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectPoints_UsingUnit", environment, "@inKeyUnitsID", inKeyUnitsID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_Units_SelectRangePresets_UsingUnits
		[HttpGet]
		[Route("api/V1/{environment}/refUnits/SelectRangePresets/{inFkUnitID}")]
		public HttpResponseMessage refUnits_SelectRangePresets(string environment, string inFkUnitID)
		{
			string tblName = "ref_Units";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectRangePresets_UsingUnits", environment, "@inFkUnitID", inFkUnitID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_TranslationValues_Get

		//qry_ref_TranslationValues_****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/refTranslationValues")]
		public HttpResponseMessage refTranslationValues_Class(string environment)
		{
			string tblName = "ref_TranslationValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_TranslationValues dataRecs = new HE_Models.Points.ref_TranslationValues();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationValues_****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/refTranslationValues/{keyTranslationValueID}")]
		public HttpResponseMessage refTranslationValues_Class(string environment, string keyTranslationValueID)
		{
			string tblName = "ref_TranslationValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyTranslationValueID", keyTranslationValueID);
				HE_Models.Points.ref_TranslationValues dataRecs = new HE_Models.Points.ref_TranslationValues();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationValues_****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/refTranslationValues")]
		public HttpResponseMessage refTranslationValues(string environment)
		{
			string tblName = "ref_TranslationValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationValues_****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/refTranslationValues/{keyTranslationValueID}")]
		public HttpResponseMessage refTranslationValues(string environment, string keyTranslationValueID)
		{
			string tblName = "ref_TranslationValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyTranslationValueID", keyTranslationValueID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationValues_SelectAll_ForTranslationType
		[HttpGet]
		[Route("api/V1/{environment}/refTranslationValues/SelectForType/{inFkTranslationTypeID}")]
		public HttpResponseMessage refTranslationValues_ForType(string environment, string inFkTranslationTypeID)
		{
			string tblName = "ref_TranslationValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll_ForTranslationType", environment, "@inFkTranslationTypeID", inFkTranslationTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationValues_SelectTranslation_ForValue
		[HttpGet]
		[Route("api/V1/{environment}/refTranslationValues/SelectForValue/{inFkTranslationTypeID}/{inTranValue_Value}")]
		public HttpResponseMessage refTranslationValues_ForValue(string environment, string inFkTranslationTypeID, string inTranValue_Value)
		{
			string tblName = "ref_TranslationValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();
				Dictionary<string, object> myDict = new Dictionary<string, object> { { "@inFkTranslationTypeID", inFkTranslationTypeID }, { "@inTranValue_Value", inTranValue_Value } };
				oDS = oDB.getData("qry_" + tblName + "_SelectTranslation_ForValue", environment, myDict);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region ref_TranslationTypes_Get

		//qry_ref_TranslationTypes_
		[HttpGet]
		[Route("api/V1/{environment}/Class/refTranslationTypes")]
		public HttpResponseMessage refTranslationTypes_Class(string environment)
		{
			string tblName = "ref_TranslationTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_TranslationTypes dataRecs = new HE_Models.Points.ref_TranslationTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationTypes_
		[HttpGet]
		[Route("api/V1/{environment}/Class/refTranslationTypes/{keyTranslationTypeID}")]
		public HttpResponseMessage refTranslationTypes_Class(string environment, string keyTranslationTypeID)
		{
			string tblName = "ref_TranslationTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyTranslationTypeID", keyTranslationTypeID);
				HE_Models.Points.ref_TranslationTypes dataRecs = new HE_Models.Points.ref_TranslationTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationTypes_
		[HttpGet]
		[Route("api/V1/{environment}/refTranslationTypes")]
		public HttpResponseMessage refTranslationTypes(string environment)
		{
			string tblName = "ref_TranslationTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationTypes_
		[HttpGet]
		[Route("api/V1/{environment}/refTranslationTypes/{keyTranslationTypeID}")]
		public HttpResponseMessage refTranslationTypes(string environment, string keyTranslationTypeID)
		{
			string tblName = "ref_TranslationTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyTranslationTypeID", keyTranslationTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationTypes_SelectPoints_UsingTranslation
		[HttpGet]
		[Route("api/V1/{environment}/refTranslationTypes/SelectPoints/{inKeyTranslationTypeID}")]
		public HttpResponseMessage refTranslationTypes_SelectPoints(string environment, string inKeyTranslationTypeID)
		{
			string tblName = "ref_TranslationTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectPointsUsingTranslation", environment, "@inKeyTranslationTypeID", inKeyTranslationTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_Recorders_Get

		//qrys DNE
		[HttpGet]
		[Route("api/V1/{environment}/Class/refRecorders")]
		public HttpResponseMessage refRecorders_Class(string environment)
		{
			string tblName = "ref_Recorders";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_Recorders dataRecs = new HE_Models.Points.ref_Recorders();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/Class/refRecorders/{keyRecorderID}")]
		public HttpResponseMessage refRecorders_Class(string environment, string keyRecorderID)
		{
			string tblName = "ref_Recorders";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRecorderID", keyRecorderID);
				HE_Models.Points.ref_Recorders dataRecs = new HE_Models.Points.ref_Recorders();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refRecorders")]
		public HttpResponseMessage refRecorders(string environment)
		{
			string tblName = "ref_Recorders";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}


		//this call actually works when given a pointID and not keyRecorderID since the SelectKey qry is based on point ID for some reason
		[HttpGet]
		[Route("api/V1/{environment}/refRecorders/{keyRecorderID}")]
		public HttpResponseMessage refRecorders(string environment, string keyRecorderID)
		{
			string tblName = "ref_Recorders";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRecorderID", keyRecorderID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region ref_RangeTypes_Get

		//qrys DNE
		[HttpGet]
		[Route("api/V1/{environment}/Class/refRangeTypes")]
		public HttpResponseMessage refRangeTypes_Class(string environment)
		{
			string tblName = "ref_RangeTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_RangeTypes dataRecs = new HE_Models.Points.ref_RangeTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/Class/refRangeTypes/{keyRangeTypeID}")]
		public HttpResponseMessage refRangeTypes_Class(string environment, string keyRangeTypeID)
		{
			string tblName = "ref_RangeTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRangeTypeID", keyRangeTypeID);
				HE_Models.Points.ref_RangeTypes dataRecs = new HE_Models.Points.ref_RangeTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refRangeTypes")]
		public HttpResponseMessage refRangeTypes(string environment)
		{
			string tblName = "ref_RangeTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refRangeTypes/{keyRangeTypeID}")]
		public HttpResponseMessage refRangeTypes(string environment, string keyRangeTypeID)
		{
			string tblName = "ref_RangeTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRangeTypeID", keyRangeTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_RangeTypes_PointsUsingRangeType
		[HttpGet]
		[Route("api/V1/{environment}/refRangeTypes/PointsUsingRangeType/{inKeyRangeTypeID}")]
		public HttpResponseMessage refRangeTypes_PointsUsingRangeType(string environment, string inKeyRangeTypeID)
		{
			string tblName = "ref_RangeTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_PointsUsingRangeType", environment, "@inKeyRangeTypeID", inKeyRangeTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_RangePresets_Get

		//qrys DNE
		[HttpGet]
		[Route("api/V1/{environment}/Class/refRangePresets")]
		public HttpResponseMessage refRangePresets_Class(string environment)
		{
			string tblName = "ref_RangePresets";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_RangePresets dataRecs = new HE_Models.Points.ref_RangePresets();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/Class/refRangePresets/{keyRangePresetID}")]
		public HttpResponseMessage refRangePresets_Class(string environment, string keyRangePresetID)
		{
			string tblName = "ref_RangePresets";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRangePresetID", keyRangePresetID);
				HE_Models.Points.ref_RangeTypes dataRecs = new HE_Models.Points.ref_RangeTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refRangePresets")]
		public HttpResponseMessage refRangePresets(string environment)
		{
			string tblName = "ref_RangePresets";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refRangePresets/{keyRangePresetID}")]
		public HttpResponseMessage refRangePresets(string environment, string keyRangePresetID)
		{
			string tblName = "ref_RangePresets";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRangePresetID", keyRangePresetID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_RangePresets_ForType_and_Unit
		[HttpGet]
		[Route("api/V1/{environment}/refRangePresets/SelectForTypeAndUnit/{in_fkRangeTypeID}/{in_fkUnitID}")]
		public HttpResponseMessage refRangePresets_ForTypeAndUnit(string environment, string in_fkRangeTypeID, string in_fkUnitID)
		{
			string tblName = "ref_RangePresets";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				Dictionary<string,object> myDict = new Dictionary<string, object> { { "@in_fkRangeTypeID", in_fkRangeTypeID }, { "@in_fkUnitID", in_fkUnitID } };

				oDS = oDB.getData("qry_" + tblName + "_ForType_and_Unit", environment, myDict );

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_PointUDFs_Get

		//qrys DNE
		[HttpGet]
		[Route("api/V1/{environment}/Class/refPointUDFs")]
		public HttpResponseMessage refPointUDFs_Class(string environment)
		{
			string tblName = "ref_PointUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_PointUDFs dataRecs = new HE_Models.Points.ref_PointUDFs();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/Class/refPointUDFs/{keyPUDF_ID}")]
		public HttpResponseMessage refPointUDFs_Class(string environment, string keyPUDF_ID)
		{
			string tblName = "ref_PointUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyPUDF_ID", keyPUDF_ID);
				HE_Models.Points.ref_PointUDFs dataRecs = new HE_Models.Points.ref_PointUDFs();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refPointUDFs")]
		public HttpResponseMessage refPointUDFs(string environment)
		{
			string tblName = "ref_PointUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refPointUDFs/{keyPUDF_ID}")]
		public HttpResponseMessage refPointUDFs(string environment, string keyPUDF_ID)
		{
			string tblName = "ref_PointUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyPUDF_ID", keyPUDF_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_PointUDFs_ForKey
		[HttpGet]
		[Route("api/V1/{environment}/refPointUDFs/ForKey/{fkPointID}")]
		public HttpResponseMessage refPointUDFs_ForKey(string environment, string fkPointID)
		{
			string tblName = "ref_PointUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_ForKey", environment, "@fkPointID", fkPointID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_DriverTypes_Get

		//qry_ref_DriverTypes_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/refDriverTypes")]
		public HttpResponseMessage refDriverTypes_Class(string environment)
		{
			string tblName = "ref_PointUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_DriverTypes dataRecs = new HE_Models.Points.ref_DriverTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DriverTypes_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/refDriverTypes/{keyDriverTypeID}")]
		public HttpResponseMessage refDriverTypes_Class(string environment, string keyDriverTypeID)
		{
			string tblName = "ref_DriverTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDriverTypeID", keyDriverTypeID);
				HE_Models.Points.ref_DriverTypes dataRecs = new HE_Models.Points.ref_DriverTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DriverTypes_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/ref_DriverTypes")]
		public HttpResponseMessage refDriverTypes(string environment)
		{
			string tblName = "ref_DriverTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DriverTypes_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/refDriverTypes/{keyDriverTypeID}")]
		public HttpResponseMessage refDriverTypes(string environment, string keyDriverTypeID)
		{
			string tblName = "ref_DriverTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDriverTypeID", keyDriverTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_DeviceUDFs_Get

		//qrys DNE
		[HttpGet]
		[Route("api/V1/{environment}/Class/refDeviceUDFs")]
		public HttpResponseMessage refDeviceUDFs_Class(string environment)
		{
			string tblName = "ref_DeviceUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_DeviceUDFs dataRecs = new HE_Models.Points.ref_DeviceUDFs();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/Class/refDeviceUDFs/{keyDUDF_ID}")]
		public HttpResponseMessage refDeviceUDFs_Class(string environment, string keyDUDF_ID)
		{
			string tblName = "ref_DeviceUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDUDF_ID", keyDUDF_ID);
				HE_Models.Points.ref_DeviceUDFs dataRecs = new HE_Models.Points.ref_DeviceUDFs();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refDeviceUDFs")]
		public HttpResponseMessage refDeviceUDFs(string environment)
		{
			string tblName = "ref_DeviceUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refDeviceUDFs/{keyDUDF_ID}")]
		public HttpResponseMessage refDeviceUDFs(string environment, string keyDUDF_ID)
		{
			string tblName = "ref_DeviceUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDUDF_ID", keyDUDF_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DeviceUDFs_ForKey
		[HttpGet]
		[Route("api/V1/{environment}/refDeviceUDFs/ForKey/{fkDeviceID}")]
		public HttpResponseMessage refDeviceUDFs_ForKey(string environment, string fkDeviceID)
		{
			string tblName = "ref_DeviceUDFs";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_ForKey", environment, "@fkDeviceID", fkDeviceID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_DeviceTypes_Get

		//qry_ref_DeviceTypes_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/refDeviceTypes")]
		public HttpResponseMessage refDeviceTypes_Class(string environment)
		{
			string tblName = "ref_DeviceTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_DeviceTypes dataRecs = new HE_Models.Points.ref_DeviceTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DeviceTypes_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/Class/refDeviceTypes/{keyDevTypeID}")]
		public HttpResponseMessage refDeviceTypes_Class(string environment, string keyDevTypeID)
		{
			string tblName = "ref_DeviceTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDevTypeID", keyDevTypeID);
				HE_Models.Points.ref_DeviceTypes dataRecs = new HE_Models.Points.ref_DeviceTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DeviceTypes_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/refDeviceTypes")]
		public HttpResponseMessage refDeviceTypes(string environment)
		{
			string tblName = "ref_DeviceTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DeviceTypes_SelectKey ****DNE****
		[HttpGet]
		[Route("api/V1/{environment}/refDeviceTypes/{keyDevTypeID}")]
		public HttpResponseMessage refDeviceTypes(string environment, string keyDevTypeID)
		{
			string tblName = "ref_DeviceTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyDevTypeID", keyDevTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DeviceTypes_ReturnDrivers_ForDAQ
		[HttpGet]
		[Route("api/V1/{environment}/refDeviceTypes/ReturnDrivers/{in_DAQ_ID}")]
		public HttpResponseMessage refDeviceTypes_ForDAQ(string environment, string in_DAQ_ID)
		{
			string tblName = "ref_DeviceTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_ReturnDrivers_ForDAQ", environment, "@in_DAQ_ID", in_DAQ_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DeviceTypes_SelectDevices_UsingDeviceType
		[HttpGet]
		[Route("api/V1/{environment}/refDeviceTypes/SelectDevices/{inKeyDevTypeID}")]
		public HttpResponseMessage refDeviceTypes_SelectDevices(string environment, string inKeyDevTypeID)
		{
			string tblName = "ref_DeviceTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectDevices_UsingDeviceType", environment, "@inKeyDevTypeID", inKeyDevTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_Distinct_DeviceTypes_ForDAQ
		[HttpGet]
		[Route("api/V1/{environment}/refDeviceTypes/SelectDistinctTypesForDAQ/{in_DAQ_ID}")]
		public HttpResponseMessage refDeviceTypes_SelectDistinctDevicesForDAQ(string environment, string in_DAQ_ID)
		{
			string tblName = "ref_Distinct_DeviceTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_ForDAQ", environment, "@in_DAQ_ID", in_DAQ_ID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DeviceTypes_ForDriver
		[HttpGet]
		[Route("api/V1/{environment}/refDeviceTypes/SelectForDriver/{infkDriverTypeID}")]
		public HttpResponseMessage refDeviceTypes_ForDriver(string environment, string infkDriverTypeID)
		{
			string tblName = "ref_DeviceTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_ForDriver", environment, "@infkDriverTypeID", infkDriverTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_BusTypes_Get

		//qrys DNE
		[HttpGet]
		[Route("api/V1/{environment}/Class/refBusTypes")]
		public HttpResponseMessage refBusTypes_Class(string environment)
		{
			string tblName = "ref_BusTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_BusTypes dataRecs = new HE_Models.Points.ref_BusTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/Class/refBusTypes/{keyBusTypeID}")]
		public HttpResponseMessage refBusTypes_Class(string environment, string keyBusTypeID)
		{
			string tblName = "ref_BusTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyBusTypeID", keyBusTypeID);
				HE_Models.Points.ref_BusTypes dataRecs = new HE_Models.Points.ref_BusTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refBusTypes")]
		public HttpResponseMessage refBusTypes(string environment)
		{
			string tblName = "ref_BusTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/refBusTypes/{keyRangePresetID}")]
		public HttpResponseMessage refBusTypes(string environment, string keyBusTypeID)
		{
			string tblName = "ref_BusTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyBusTypeID", keyBusTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_BusTypes_SelectDAQs_UsingBusType
		[HttpGet]
		[Route("api/V1/{environment}/refBusTypes/SelectDAQs/{keyBusTypeID}")]
		public HttpResponseMessage refBusTypes_ForDAQ(string environment, string keyBusTypeID)
		{
			string tblName = "ref_BusTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectDAQs_UsingBusType", environment, "@inKeyBusTypeID", keyBusTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region lst_RecorderPoints

		//qrys DNE
		[HttpGet]
		[Route("api/V1/{environment}/Class/lstRecorderPoints")]
		public HttpResponseMessage lstRecorderPoints_Class(string environment)
		{
			string tblName = "lst_RecorderPoints";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/Class/lstRecorderPoints/{keyRecorderPointID}")]
		public HttpResponseMessage lstRecorderPoints_Class(string environment, string keyRecorderPointID)
		{
			string tblName = "lst_RecorderPoints";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRecorderPointID", keyRecorderPointID);
				HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/lstRecorderPoints")]
		public HttpResponseMessage lstRecorderPoints(string environment)
		{
			string tblName = "lst_RecorderPoints";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/lstRecorderPoints/{keyRecorderPointID}")]
		public HttpResponseMessage lstRecorderPoints(string environment, string keyRecorderPointID)
		{
			string tblName = "lst_RecorderPoints";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRecorderPointID", keyRecorderPointID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_lst_RecorderPoints_SelectRecorders_ForPoint
		[HttpGet]
		[Route("api/V1/{environment}/lstRecorderPoints/SelectRecordersForPoint/{inFkPointID}")]
		public HttpResponseMessage lstRecorderPoints_ForPoint(string environment, string inFkPointID)
		{
			string tblName = "lst_RecorderPoints";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectRecorders_ForPoint", environment, "@inFkPointID", inFkPointID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_lst_RecorderPoints_SelectPoints_InRecorder
		[HttpGet]
		[Route("api/V1/{environment}/lstRecorderPoints/SelectPointsForRecorder/{inKeyRecorderID}")]
		public HttpResponseMessage lstRecorderPoints_ForRecorder(string environment, string inKeyRecorderID)
		{
			string tblName = "lst_RecorderPoints";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectPoints_InRecorder", environment, "@inKeyRecorderID", inKeyRecorderID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_CalibrationValuesTypes

		//qry_ref_CalibrationValuesTypes_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/refCalibrationValuesTypes")]
		public HttpResponseMessage refCalibrationValuesTypes_Class(string environment)
		{
			string tblName = "ref_CalibrationValuesTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.ref_CalibrationValuesTypes dataRecs = new HE_Models.Points.ref_CalibrationValuesTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_CalibrationValuesTypes_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/Class/refCalibrationValuesTypes/{keyCalibrationValuesTypeID}")]
		public HttpResponseMessage tbl_CalibrationValuesTypes_Class(string environment, string keyCalibrationValuesTypeID)
		{
			string tblName = "ref_CalibrationValuesTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyCalibrationValuesTypeID", keyCalibrationValuesTypeID);
				HE_Models.Points.ref_CalibrationValuesTypes dataRecs = new HE_Models.Points.ref_CalibrationValuesTypes();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_CalibrationValuesTypes_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/refCalibrationValuesTypes")]
		public HttpResponseMessage refCalibrationValuesTypes(string environment)
		{
			string tblName = "ref_CalibrationValuesTypes";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_CalibrationValuesTypes_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/refCalibrationValuesTypes/{keyRecorderPointID}")]
		public HttpResponseMessage refCalibrationValuesTypes(string environment, string keyCalibrationValuesTypeID)
		{
			string tblName = "lst_RecorderPoints";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyRecorderPointID", keyCalibrationValuesTypeID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_CalibrationLists
		
		//qrys DNE
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblCalibrationLists")]
		public HttpResponseMessage tblCalibrationLists_Class(string environment)
		{
			string tblName = "tbl_CalibrationLists";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_CalibrationLists dataRecs = new HE_Models.Points.tbl_CalibrationLists();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/Class/tblCalibrationLists/{keyCalibrationListID}")]
		public HttpResponseMessage tblCalibrationLists_Class(string environment, string keyCalibrationListID)
		{
			string tblName = "tbl_CalibrationLists";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyCalibrationListID", keyCalibrationListID);
				HE_Models.Points.tbl_CalibrationLists dataRecs = new HE_Models.Points.tbl_CalibrationLists();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/tblCalibrationLists")]
		public HttpResponseMessage tblCalibrationLists(string environment)
		{
			string tblName = "tbl_CalibrationLists";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpGet]
		[Route("api/V1/{environment}/tblCalibrationLists/{keyCalibrationListID}")]
		public HttpResponseMessage tblCalibrationLists(string environment, string keyCalibrationListID)
		{
			string tblName = "tbl_CalibrationLists";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyCalibrationListID", keyCalibrationListID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_CalibrationRows

		//qry_tbl_CalibrationRows_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblCalibrationRows")]
		public HttpResponseMessage tblCalibrationRows_Class(string environment)
		{
			string tblName = "tbl_CalibrationRows";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_CalibrationRows dataRecs = new HE_Models.Points.tbl_CalibrationRows();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_CalibrationRows_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblCalibrationRows/{keyCalibrationRowID}")]
		public HttpResponseMessage tblCalibrationRows_Class(string environment, string keyCalibrationRowID)
		{
			string tblName = "tbl_CalibrationRows";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyCalibrationRowID", keyCalibrationRowID);
				HE_Models.Points.tbl_CalibrationRows dataRecs = new HE_Models.Points.tbl_CalibrationRows();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_CalibrationRows_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblCalibrationRows")]
		public HttpResponseMessage tblCalibrationRows(string environment)
		{
			string tblName = "tbl_CalibrationLists";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_CalibrationRows_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/tblCalibrationRows/{keyCalibrationRowID}")]
		public HttpResponseMessage tblCalibrationRows(string environment, string keyCalibrationRowID)
		{
			string tblName = "tbl_CalibrationRows";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyCalibrationRowID", keyCalibrationRowID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_HistoricPointDatas

		//qry_tbl_HistoricPointDatas_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblHistoricPointDatas")]
		public HttpResponseMessage tblHistoricPointDatas_Class(string environment)
		{
			string tblName = "tbl_HistoricPointDatas";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_HistoricPointDatas dataRecs = new HE_Models.Points.tbl_HistoricPointDatas();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointDatas_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblHistoricPointDatas/{keyHistoricPointData}")]
		public HttpResponseMessage tblHistoricPointDatas_Class(string environment, string keyHistoricPointData)
		{
			string tblName = "tbl_HistoricPointDatas";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyHistoricPointData", keyHistoricPointData);
				HE_Models.Points.tbl_HistoricPointDatas dataRecs = new HE_Models.Points.tbl_HistoricPointDatas();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointDatas_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblHistoricPointDatas")]
		public HttpResponseMessage tblHistoricPointDatas(string environment)
		{
			string tblName = "tbl_HistoricPointDatas";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointDatas_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/tblHistoricPointDatas/{keyHistoricPointData}")]
		public HttpResponseMessage tblHistoricPointDatas(string environment, string keyHistoricPointData)
		{
			string tblName = "tbl_HistoricPointDatas";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyHistoricPointData", keyHistoricPointData);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_HistoricPointUniqueValues

		//qry_tbl_HistoricPointUniqueValues_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblHistoricPointUniqueValues")]
		public HttpResponseMessage tblHistoricPointUniqueValues_Class(string environment)
		{
			string tblName = "tbl_HistoricPointUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);
				HE_Models.Points.tbl_HistoricPointUniqueValues dataRecs = new HE_Models.Points.tbl_HistoricPointUniqueValues();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointUniqueValues_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/Class/tblHistoricPointUniqueValues/{keyHistoricPointUniqueValueID}")]
		public HttpResponseMessage tbl_HistoricPointUniqueValues_Class(string environment, string keyHistoricPointUniqueValueID)
		{
			string tblName = "tbl_HistoricPointUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyHistoricPointUniqueValueID", keyHistoricPointUniqueValueID);
				HE_Models.Points.tbl_HistoricPointUniqueValues dataRecs = new HE_Models.Points.tbl_HistoricPointUniqueValues();
				dataRecs = clsFunc.dt2cls(oDS.Tables[0], dataRecs);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, dataRecs);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointUniqueValues_SelectAll
		[HttpGet]
		[Route("api/V1/{environment}/tblHistoricPointUniqueValues")]
		public HttpResponseMessage tblHistoricPointUniqueValues(string environment)
		{
			string tblName = "tbl_HistoricPointDatas";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectAll", environment);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointUniqueValues_SelectKey
		[HttpGet]
		[Route("api/V1/{environment}/tblHistoricPointUniqueValues/{keyHistoricPointUniqueValueID}")]
		public HttpResponseMessage tblHistoricPointUniqueValues(string environment, string keyHistoricPointUniqueValueID)
		{
			string tblName = "tbl_HistoricPointUniqueValues";
			Boolean bValid = true;
			if (bValid)
			{
				DataSet oDS = new DataSet();

				oDS = oDB.getData("qry_" + tblName + "_SelectKey", environment, "@keyHistoricPointUniqueValueID", keyHistoricPointUniqueValueID);

				return Request.CreateResponse(System.Net.HttpStatusCode.OK, oDS);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#endregion

		//***********************************	POST  ***********************************	

		#region POST

		#region tbl_Devices_Post

		//qrys DME
		[HttpPost]
		[Route("api/V1/{environ}/tblDevices")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_Devices dataRecs) //how does dataRecs get passed into this method as a parameter? 
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";

				//oDB is a dbFunctions object
				//we call the processSP method, passing in enviroinment string, sAction string and dataRecs
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		
		//qry_tbl_Devices_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblDevicesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_Device dataRec)
		{

			HE_Models.Points.tbl_Devices dataRecs = new HE_Models.Points.tbl_Devices();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_PointUniqueValues_Post

		//qrys DNE
		[HttpPost]
		[Route("api/V1/{environ}/tblPointUniqueValues")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_PointUniqueValues dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPost]
		[Route("api/V1/{environ}/tblPointUniqueValuesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_PointUniqueValue dataRec)
		{

			HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_PointUniqueValues_AddField
		[HttpPost]
		[Route("api/V1/{environ}/tblPointUniqueValues/AddField/{inKeyPUDF_ID}")]
		public HttpResponseMessage post_AddField(string inKeyPUDF_ID)
		{

			HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
			HE_Models.Points.tbl_PointUniqueValue dataRec = new HE_Models.Points.tbl_PointUniqueValue() { 
				fkPUDF_ID = inKeyPUDF_ID
			};
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "ADDFIELD", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_PointUniqueValues_AddPoint
		[HttpPost]
		[Route("api/V1/{environ}/tblPointUniqueValues/AddPoint/{inFkPointID}/{inFkDevTypeID}")]
		public HttpResponseMessage post_AddPoint(string inFkPointID, string inFkDevTypeID)
		{

			HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
			HE_Models.Points.tbl_PointUniqueValue dataRec = new HE_Models.Points.tbl_PointUniqueValue()
			{
				fkPointID = inFkPointID,
				fkPUDF_ID = inFkDevTypeID //wrong field one could argue, but functionality works
			};
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "ADDPOINT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_Points_Post
		[HttpPost]
		[Route("api/V1/{environ}/tblPoints")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_Points dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblPointsFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_Point dataRec)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_Points_InsertOrUpdate
		[HttpPost]
		[Route("api/V1/{environ}/tblPoints/InsertOrUpdate")]
		public HttpResponseMessage post_InsertOrUpdate([FromBody] HE_Models.Points.tbl_Point dataRec)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERTORUPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_PointRanges_Post
		[HttpPost]
		[Route("api/V1/{environ}/tblPointRanges")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_PointRanges dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointRanges_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblPointRangesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_PointRange dataRec)
		{

			HE_Models.Points.tbl_PointRanges dataRecs = new HE_Models.Points.tbl_PointRanges();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_DeviceUniqueValues_Post
		[HttpPost]
		[Route("api/V1/{environ}/tblDeviceUniqueValues")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_DeviceUniqueValues dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPost]
		[Route("api/V1/{environ}/tblDeviceUniqueValuesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_DeviceUniqueValue dataRec)
		{

			HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_DeviceUniqueValues_AddDevice (from body version)
		[HttpPost]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/AddDevice")]
		public HttpResponseMessage postPartial_Device([FromBody] HE_Models.Points.tbl_DeviceUniqueValue dataRec)
		{

			HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
			dataRecs.Add(dataRec);
			//dataRec will contain a partially filled object with fkDeviceID and fkDUDF_ID filled

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "ADDDEVICE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_DeviceUniqueValues_AddDevice 
		[HttpPost]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/AddDevice/{inFkDeviceID}/{inFkDevTypeID}")]
		public HttpResponseMessage post_AddDevice(string inFkDeviceID, string inFkDevTypeID)
		{

			HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
			HE_Models.Points.tbl_DeviceUniqueValue dataRec = new HE_Models.Points.tbl_DeviceUniqueValue() { 
				fkDeviceID = inFkDeviceID,
				fkDUDF_ID = inFkDevTypeID
			};
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "ADDDEVICE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_DeviceUniqueValues_AddField (from body version)
		[HttpPost]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/AddField")]
		public HttpResponseMessage postPartial_Field([FromBody] HE_Models.Points.tbl_DeviceUniqueValue dataRec)
		{

			HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
			dataRecs.Add(dataRec);
			//dataRec will contain a partially filled object with fkDUDF_ID filled

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "ADDFIELD", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_DeviceUniqueValues_AddField
		[HttpPost]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/AddField/{inKeyDUDF_ID}")]
		public HttpResponseMessage post_AddField_DUV(string inKeyDUDF_ID)
		{

			HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
			HE_Models.Points.tbl_DeviceUniqueValue dataRec = new HE_Models.Points.tbl_DeviceUniqueValue() { 
				fkDUDF_ID = inKeyDUDF_ID
			};
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "ADDFIELD", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_DAQ_Post

		[HttpPost]
		[Route("api/V1/{environ}/tblDAQ")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_DAQs dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DAQ_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblDAQFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_DAQ dataRec)
		{

			HE_Models.Points.tbl_DAQs dataRecs = new HE_Models.Points.tbl_DAQs();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_Buses_Post
		[HttpPost]
		[Route("api/V1/{environ}/tblBuses")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_Buses dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Buses_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblBusesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_Bus dataRec)
		{

			HE_Models.Points.tbl_Buses dataRecs = new HE_Models.Points.tbl_Buses();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_Versions_Post

		//qrys DNE
		[HttpPost]
		[Route("api/V1/{environ}/refVersions")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_Versions dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPost]
		[Route("api/V1/{environ}/refVersionFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_Version dataRec)
		{

			HE_Models.Points.ref_Versions dataRecs = new HE_Models.Points.ref_Versions();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_Units_Post
		[HttpPost]
		[Route("api/V1/{environ}/refUnits")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_Units dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_Units_Insert
		[HttpPost]
		[Route("api/V1/{environ}/refUnitsFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_Unit dataRec)
		{

			HE_Models.Points.ref_Units dataRecs = new HE_Models.Points.ref_Units();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_TranslationValues_Post

		//qrys DNE
		[HttpPost]
		[Route("api/V1/{environ}/refTranslationValues")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_TranslationValues dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPost]
		[Route("api/V1/{environ}/refTranslationValues")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_TranslationValue dataRec)
		{

			HE_Models.Points.ref_TranslationValues dataRecs = new HE_Models.Points.ref_TranslationValues();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_TranslationTypes_Post

		[HttpPost]
		[Route("api/V1/{environ}/refTranslationTypes")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_TranslationTypes dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationTypes_Insert
		[HttpPost]
		[Route("api/V1/{environ}/refTranslationTypesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_TranslationType dataRec)
		{

			HE_Models.Points.ref_TranslationTypes dataRecs = new HE_Models.Points.ref_TranslationTypes();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_Recorders_Post

		//qrys DNE
		[HttpPost]
		[Route("api/V1/{environ}/refRecorders")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_Recorders dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPost]
		[Route("api/V1/{environ}/refRecordersFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_Recorder dataRec)
		{

			HE_Models.Points.ref_Recorders dataRecs = new HE_Models.Points.ref_Recorders();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_RangeTypes_Post

		//qrys DNE
		[HttpPost]
		[Route("api/V1/{environ}/refRangeTypes")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_RangeTypes dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPost]
		[Route("api/V1/{environ}/refRangeTypesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_RangeType dataRec)
		{

			HE_Models.Points.ref_RangeTypes dataRecs = new HE_Models.Points.ref_RangeTypes();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_RangPresets_Post

		//qrys DNE
		[HttpPost]
		[Route("api/V1/{environ}/refRangePresets")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_RangePresets dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPost]
		[Route("api/V1/{environ}/refRangePresetsFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_RangePreset dataRec)
		{

			HE_Models.Points.ref_RangePresets dataRecs = new HE_Models.Points.ref_RangePresets();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_PointUDFs_Post

		//qrys DNE
		[HttpPost]
		[Route("api/V1/{environ}/refPointUDFs")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_PointUDFs dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPost]
		[Route("api/V1/{environ}/refPointUDFsFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_PointUDF dataRec)
		{

			HE_Models.Points.ref_PointUDFs dataRecs = new HE_Models.Points.ref_PointUDFs();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_DriverTypes_Post
		[HttpPost]
		[Route("api/V1/{environ}/refDriverTypes")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_DriverTypes dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPost]
		[Route("api/V1/{environ}/refDriverTypesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_DriverType dataRec)
		{

			HE_Models.Points.ref_DriverTypes dataRecs = new HE_Models.Points.ref_DriverTypes();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_DeviceUDFs_Post
		[HttpPost]
		[Route("api/V1/{environ}/refDeviceUDFs")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_DeviceUDFs dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPost]
		[Route("api/V1/{environ}/refDeviceUDFsFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_DeviceUDF dataRec)
		{

			HE_Models.Points.ref_DeviceUDFs dataRecs = new HE_Models.Points.ref_DeviceUDFs();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_DeviceTypes_Post
		[HttpPost]
		[Route("api/V1/{environ}/refDeviceTypes")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_DeviceTypes dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPost]
		[Route("api/V1/{environ}/refDeviceTypesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_DeviceType dataRec)
		{

			HE_Models.Points.ref_DeviceTypes dataRecs = new HE_Models.Points.ref_DeviceTypes();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_BusTypes_Post
		[HttpPost]
		[Route("api/V1/{environ}/refBusTypes")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_BusTypes dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPost]
		[Route("api/V1/{environ}/refBusTypesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_BusType dataRec)
		{

			HE_Models.Points.ref_BusTypes dataRecs = new HE_Models.Points.ref_BusTypes();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region lst_RecorderPoints_Post
		[HttpPost]
		[Route("api/V1/{environ}/lstRecorderPoints")]
		public HttpResponseMessage post(string environ, HE_Models.Points.lst_RecorderPoints dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPost]
		[Route("api/V1/{environ}/lstRecorderPointsFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.lst_RecorderPoint dataRec)
		{

			HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_lst_RecorderPoints_InsertRecorderPoint (from body version)
		[HttpPost]
		[Route("api/V1/{environ}/lstRecorderPoints/InsertRecorderPoint")]
		public HttpResponseMessage postPartial([FromBody] HE_Models.Points.lst_RecorderPoint dataRec)
		{

			HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
			dataRecs.Add(dataRec);
			//dataRec will contain a partially filled recorderPoint object with fkRecorderID and fkPointID filled

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERTRECORDERPOINT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_lst_RecorderPoints_InsertRecorderPoint
		[HttpPost]
		[Route("api/V1/{environ}/lstRecorderPoints/InsertRecorderPoint/{inFkRecorderID}/{inFkPointID}")]
		public HttpResponseMessage post_InsertRecorderPoint(string inFkRecorderID, string inFkPointID)
		{

			HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
			HE_Models.Points.lst_RecorderPoint dataRec = new HE_Models.Points.lst_RecorderPoint() { 
				fkPointID = inFkPointID,
				fkRecorderID = inFkRecorderID
			};
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERTRECORDERPOINT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_CalibrationValuesTypes

		[HttpPost]
		[Route("api/V1/{environ}/refCalibrationValuesTypes")]
		public HttpResponseMessage post(string environ, HE_Models.Points.ref_CalibrationValuesTypes dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_CalibrationValuesTypes_Insert
		[HttpPost]
		[Route("api/V1/{environ}/refCalibrationValuesTypesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.ref_CalibrationValuesType dataRec)
		{

			HE_Models.Points.ref_CalibrationValuesTypes dataRecs = new HE_Models.Points.ref_CalibrationValuesTypes();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_CalibrationLists

		[HttpPost]
		[Route("api/V1/{environ}/tblCalibrationLists")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_CalibrationLists dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_CalibrationLists_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblCalibrationListsFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_CalibrationList dataRec)
		{

			HE_Models.Points.tbl_CalibrationLists dataRecs = new HE_Models.Points.tbl_CalibrationLists();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_CalibrationRows

		[HttpPost]
		[Route("api/V1/{environ}/tblCalibrationRows")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_CalibrationRows dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_CalibrationRows_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblCalibrationRowsFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_CalibrationRow dataRec)
		{

			HE_Models.Points.tbl_CalibrationRows dataRecs = new HE_Models.Points.tbl_CalibrationRows();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_HistoricPointDatas

		[HttpPost]
		[Route("api/V1/{environ}/tblHistoricPointDatas")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_HistoricPointDatas dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointDatas_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblHistoricPointDatasFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_HistoricPointData dataRec)
		{

			HE_Models.Points.tbl_HistoricPointDatas dataRecs = new HE_Models.Points.tbl_HistoricPointDatas();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_HistoricPointUniqueValues

		[HttpPost]
		[Route("api/V1/{environ}/tblHistoricPointUniqueValues")]
		public HttpResponseMessage post(string environ, HE_Models.Points.tbl_HistoricPointUniqueValues dataRecs)
		{
			string sAction = "INSERT";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointUniqueValues_Insert
		[HttpPost]
		[Route("api/V1/{environ}/tblHistoricPointUniqueValuesFB")]
		public HttpResponseMessage post([FromBody] HE_Models.Points.tbl_HistoricPointUniqueValue dataRec)
		{

			HE_Models.Points.tbl_HistoricPointUniqueValues dataRecs = new HE_Models.Points.tbl_HistoricPointUniqueValues();
			dataRecs.Add(dataRec);

			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "INSERT", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		#endregion

		#endregion

		//***********************************	PUT  ***********************************	

		#region PUT

		#region tbl_PointUniqueValues_Put

		[HttpPut]
		[Route("api/V1/{environ}/tblPointUniqueValues")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_PointUniqueValues dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		[HttpPut]
		[Route("api/V1/{environ}/tblPointUniqueValuesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_PointUniqueValue dataRec)
		{

			HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_PointUniqueValues_ResetDefault_ForPoint
		[HttpPut]
		[Route("api/V1/{environ}/tblPointUniqueValues/ResetDefaultForPoint/{inFkPointID}")]
		public HttpResponseMessage put_ResetDefault(string inFkPointID)
		{
			string sAction = "RESETDEFAULT_FORPOINT";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
				HE_Models.Points.tbl_PointUniqueValue dataRec = new HE_Models.Points.tbl_PointUniqueValue() { 
					fkPointID = inFkPointID
				};
				dataRecs.Add(dataRec);

				string errorMsg = "";
				errorMsg = oDB.processSP("Points", sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointUniqueValues_UpdateValue
		[HttpPut]
		[Route("api/V1/{environ}/tblPointUniqueValues/UpdateValue/{inKeyPointUV_ID}/{inPointUV_strValue}")]
		public HttpResponseMessage put_UpdateValue(string inKeyPointUV_ID, string inPointUV_strValue)
		{
			string sAction = "UPDATEVALUE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
				HE_Models.Points.tbl_PointUniqueValue dataRec = new HE_Models.Points.tbl_PointUniqueValue()
				{
					keyPointUV_ID = inKeyPointUV_ID,
					PointUV_strValue = inPointUV_strValue
				};
				dataRecs.Add(dataRec);

				string errorMsg = "";
				errorMsg = oDB.processSP("Points", sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointUniqueValues_UpdateValue_ForForeignKeys
		[HttpPut]
		[Route("api/V1/{environ}/tblPointUniqueValues/UpdateValueFK/{inFkPointID}/{inFkPUDF_ID}/{inPointUV_strValue}")]
		public HttpResponseMessage put_UpdateValueFK(string inFkPointID, string inFkPUDF_ID, string inPointUV_strValue)
		{
			string sAction = "UPDATEVALUE_FORFOREIGNKEYS";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
				HE_Models.Points.tbl_PointUniqueValue dataRec = new HE_Models.Points.tbl_PointUniqueValue()
				{
					fkPointID = inFkPointID,
					fkPUDF_ID = inFkPUDF_ID,
					PointUV_strValue = inPointUV_strValue
				};
				dataRecs.Add(dataRec);

				string errorMsg = "";
				errorMsg = oDB.processSP("Points", sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_Devices_Put

		[HttpPut]
		[Route("api/V1/{environ}/tblDevices")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_Devices dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Devices_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblDevicesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_Device dataRec)
		{

			HE_Models.Points.tbl_Devices dataRecs = new HE_Models.Points.tbl_Devices();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_Points_Put

		[HttpPut]
		[Route("api/V1/{environ}/tblPoints")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_Points dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Points_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblPointsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_Point dataRec)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_Points_UpdateCurrentValue
		[HttpPut]
		[Route("api/V1/{environ}/tblPoints/UpdateCurrentValue/{inKeyPointID}/{inPoint_realCurrentValue}")]
		public HttpResponseMessage put_CurrentValue(string inKeyPointID, float inPoint_realCurrentValue)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			HE_Models.Points.tbl_Point dataRec = new HE_Models.Points.tbl_Point()
			{
				keyPointID = inKeyPointID,
				Point_realCurrentValue = inPoint_realCurrentValue
			};
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATECURRENTVALUE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_Points_SetFutureValueAndPending
		[HttpPut]
		[Route("api/V1/{environ}/tblPoints/SetFutureValueAndPending/{inKeyPointID}/{inPoint_singleFutureValue}")]
		public HttpResponseMessage put_FutureValue(string inKeyPointID, float inPoint_singleFutureValue)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			HE_Models.Points.tbl_Point dataRec = new HE_Models.Points.tbl_Point()
			{
				keyPointID = inKeyPointID,
				Point_singleFutureValue = inPoint_singleFutureValue
			};
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "SETFUTUREVALUEANDPENDING", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_Points_ClearPending
		[HttpPut]
		[Route("api/V1/{environ}/tblPoints/ClearPending/{inKeyPointID}")]
		public HttpResponseMessage put_ClearPending(string inKeyPointID)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			HE_Models.Points.tbl_Point dataRec = new HE_Models.Points.tbl_Point()
			{
				keyPointID = inKeyPointID,
			};
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "CLEARPENDING", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_Points_UpdateAllValues
		[HttpPut]
		[Route("api/V1/{environ}/tblPoints/UpdateAllValues/{inKeyPointID}/{inPoint_value}")]
		public HttpResponseMessage put_AllValues(string inKeyPointID, float inPoint_value)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			HE_Models.Points.tbl_Point dataRec = new HE_Models.Points.tbl_Point()
			{
				keyPointID = inKeyPointID,
				Point_realCurrentValue = inPoint_value,
				Point_realRawValue = inPoint_value,
				Point_realDisplayValue = inPoint_value
			};
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATEALLVALUES", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_Points_UpdateCurrentValue_FromBody (uses qry_tbl_Points_Update s.p. )
		[HttpPut]
		[Route("api/V1/{environ}/tblPoints/UpdateCurrentValueFB/{inPoint_realCurrentValue}")]
		public HttpResponseMessage put_CurrentValueFB([FromBody] HE_Models.Points.tbl_Point dataRec, float inPoint_realCurrentValue)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			dataRec.Point_realCurrentValue = inPoint_realCurrentValue;
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_Points_UpdateAllValues_FromBody (uses qry_tbl_Points_Update s.p. )
		[HttpPut]
		[Route("api/V1/{environ}/tblPoints/UpdateAllValuesFB/{inPoint_value}")]
		public HttpResponseMessage put_AllValuesFB([FromBody] HE_Models.Points.tbl_Point dataRec, float inPoint_value)
		{

			HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
			dataRec.Point_realCurrentValue = inPoint_value;
			dataRec.Point_realDisplayValue = inPoint_value;
			dataRec.Point_realRawValue = inPoint_value;
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_PointRanges_Put

		[HttpPut]
		[Route("api/V1/{environ}/tblPointRanges")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_PointRanges dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointRanges_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblPointRangesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_PointRange dataRec)
		{
			HE_Models.Points.tbl_PointRanges dataRecs = new HE_Models.Points.tbl_PointRanges();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_DeviceUniqueValues_Put
		[HttpPut]
		[Route("api/V1/{environ}/tblDeviceUniqueValues")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_DeviceUniqueValues dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DeviceUniqueValues_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblDeviceUniqueValuesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_DeviceUniqueValue dataRec)
		{

			HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		//qry_tbl_DeviceUniqueValues_UpdateValue
		[HttpPut]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/UpdateValue/{inKeyDevUV_ID}/{inDevUV_strValue}")]
		public HttpResponseMessage put_Value(string environ, string inKeyDevUV_ID, string inDevUV_strValue)
		{
			string sAction = "UPDATEVALUE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
				HE_Models.Points.tbl_DeviceUniqueValue dataRec = new HE_Models.Points.tbl_DeviceUniqueValue() { 
					keyDevUV_ID = inKeyDevUV_ID,
					DevUV_strValue = inDevUV_strValue
				};
				dataRecs.Add(dataRec);

				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DeviceUniqueValues_UpdateValue_ForForeignKeys
		[HttpPut]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/UpdateValue/{inFkDeviceID}/{inKeyDevUV_ID}/{inDevUV_strValue}")]
		public HttpResponseMessage put_Value_Fk(string environ, string inFkDeviceID, string inKeyDevUV_ID, string inDevUV_strValue)
		{
			string sAction = "UPDATEVALUE_FORFOREIGNKEYS";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
				HE_Models.Points.tbl_DeviceUniqueValue dataRec = new HE_Models.Points.tbl_DeviceUniqueValue()
				{
					fkDeviceID = inFkDeviceID,
					keyDevUV_ID = inKeyDevUV_ID,
					DevUV_strValue = inDevUV_strValue
				};
				dataRecs.Add(dataRec);

				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region tbl_DAQ_Put
		[HttpPut]
		[Route("api/V1/{environ}/tblDAQ")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_DAQs dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DAQ_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblDAQFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_DAQ dataRec)
		{

			HE_Models.Points.tbl_DAQs dataRecs = new HE_Models.Points.tbl_DAQs();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_Buses_Put
		[HttpPut]
		[Route("api/V1/{environ}/tblBuses")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_Buses dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Buses_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblBusesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_Bus dataRec)
		{

			HE_Models.Points.tbl_Buses dataRecs = new HE_Models.Points.tbl_Buses();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_Versions_Put
		[HttpPut]
		[Route("api/V1/{environ}/refVersions")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_Versions dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refVersionsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_Version dataRec)
		{

			HE_Models.Points.ref_Versions dataRecs = new HE_Models.Points.ref_Versions();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_Units_Put
		[HttpPut]
		[Route("api/V1/{environ}/refUnits")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_Units dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_Units_Update
		[HttpPut]
		[Route("api/V1/{environ}/refUnitsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_Unit dataRec)
		{

			HE_Models.Points.ref_Units dataRecs = new HE_Models.Points.ref_Units();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_TranslationValues_Put
		[HttpPut]
		[Route("api/V1/{environ}/refTranslationValues")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_TranslationValues dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refTranslationValuesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_TranslationValue dataRec)
		{

			HE_Models.Points.ref_TranslationValues dataRecs = new HE_Models.Points.ref_TranslationValues();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_TranslationTypes_Put
		[HttpPut]
		[Route("api/V1/{environ}/refTranslationTypes")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_TranslationTypes dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationTypes_Update
		[HttpPut]
		[Route("api/V1/{environ}/refTranslationTypesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_TranslationType dataRec)
		{

			HE_Models.Points.ref_TranslationTypes dataRecs = new HE_Models.Points.ref_TranslationTypes();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_Recorders_Put
		[HttpPut]
		[Route("api/V1/{environ}/refRecorders")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_Recorders dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refRecordersFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_Recorder dataRec)
		{

			HE_Models.Points.ref_Recorders dataRecs = new HE_Models.Points.ref_Recorders();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_RangeTypes_Put
		[HttpPut]
		[Route("api/V1/{environ}/refRangeTypes")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_RangeTypes dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refRangeTypesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_RangeType dataRec)
		{

			HE_Models.Points.ref_RangeTypes dataRecs = new HE_Models.Points.ref_RangeTypes();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_RangePresets_Put
		[HttpPut]
		[Route("api/V1/{environ}/refRangePresets")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_RangePresets dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refRangePresetsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_RangePreset dataRec)
		{

			HE_Models.Points.ref_RangePresets dataRecs = new HE_Models.Points.ref_RangePresets();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_PointUDFs_Put
		[HttpPut]
		[Route("api/V1/{environ}/refRangePresets")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_PointUDFs dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refRangePresetsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_PointUDF dataRec)
		{

			HE_Models.Points.ref_PointUDFs dataRecs = new HE_Models.Points.ref_PointUDFs();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_DriverTypes_Put
		[HttpPut]
		[Route("api/V1/{environ}/refDriverTypes")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_DriverTypes dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refDriverTypesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_DriverType dataRec)
		{

			HE_Models.Points.ref_DriverTypes dataRecs = new HE_Models.Points.ref_DriverTypes();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_DeviceUDFs_Put
		[HttpPut]
		[Route("api/V1/{environ}/refDeviceUDFs")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_DeviceUDFs dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refDeviceUDFsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_DeviceUDF dataRec)
		{

			HE_Models.Points.ref_DeviceUDFs dataRecs = new HE_Models.Points.ref_DeviceUDFs();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_DeviceTypes_Put
		[HttpPut]
		[Route("api/V1/{environ}/refDeviceTypes")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_DeviceTypes dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refDeviceTypesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_DeviceType dataRec)
		{

			HE_Models.Points.ref_DeviceTypes dataRecs = new HE_Models.Points.ref_DeviceTypes();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_BusTypes_Put
		[HttpPut]
		[Route("api/V1/{environ}/refBusTypes")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_BusTypes dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/refBusTypesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_BusType dataRec)
		{

			HE_Models.Points.ref_BusTypes dataRecs = new HE_Models.Points.ref_BusTypes();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region lst_RecorderPoints_Put
		[HttpPut]
		[Route("api/V1/{environ}/lstRecorderPoints")]
		public HttpResponseMessage put(string environ, HE_Models.Points.lst_RecorderPoints dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		[HttpPut]
		[Route("api/V1/{environ}/lstRecorderPointsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.lst_RecorderPoint dataRec)
		{

			HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region ref_CalibrationValuesTypes

		[HttpPut]
		[Route("api/V1/{environ}/refCalibrationValuesTypes")]
		public HttpResponseMessage put(string environ, HE_Models.Points.ref_CalibrationValuesTypes dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_CalibrationValuesTypes_Update
		[HttpPut]
		[Route("api/V1/{environ}/refCalibrationValuesTypesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.ref_CalibrationValuesType dataRec)
		{

			HE_Models.Points.ref_CalibrationValuesTypes dataRecs = new HE_Models.Points.ref_CalibrationValuesTypes();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_CalibrationLists

		[HttpPut]
		[Route("api/V1/{environ}/tblCalibrationLists")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_CalibrationLists dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_CalibrationLists_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblCalibrationListsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_CalibrationList dataRec)
		{

			HE_Models.Points.tbl_CalibrationLists dataRecs = new HE_Models.Points.tbl_CalibrationLists();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}

		#endregion

		#region tbl_CalibrationRows

		[HttpPut]
		[Route("api/V1/{environ}/tblCalibrationRows")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_CalibrationRows dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_CalibrationRows_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblCalibrationRowsFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_CalibrationRow dataRec)
		{

			HE_Models.Points.tbl_CalibrationRows dataRecs = new HE_Models.Points.tbl_CalibrationRows();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_HistoricPointDatas
		[HttpPut]
		[Route("api/V1/{environ}/tblHistoricPointDatas")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_HistoricPointDatas dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoircPointDatas_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblHistoricPointDatasFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_HistoricPointData dataRec)
		{

			HE_Models.Points.tbl_HistoricPointDatas dataRecs = new HE_Models.Points.tbl_HistoricPointDatas();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#region tbl_HistoricPointUniqueValues
		[HttpPut]
		[Route("api/V1/{environ}/tblHistoricPointUniqueValues")]
		public HttpResponseMessage put(string environ, HE_Models.Points.tbl_HistoricPointUniqueValues dataRecs)
		{
			string sAction = "UPDATE";
			Boolean bValid = true;
			if (bValid)
			{
				string errorMsg = "";
				errorMsg = oDB.processSP(environ, sAction, dataRecs);
				if (errorMsg == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}


			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_HistoricPointUniqueValues_Update
		[HttpPut]
		[Route("api/V1/{environ}/tblHistoricPointUniqueValuesFB")]
		public HttpResponseMessage put([FromBody] HE_Models.Points.tbl_HistoricPointUniqueValue dataRec)
		{

			HE_Models.Points.tbl_HistoricPointUniqueValues dataRecs = new HE_Models.Points.tbl_HistoricPointUniqueValues();
			dataRecs.Add(dataRec);


			string errorMsg = "";
			errorMsg = oDB.processSP("Points", "UPDATE", dataRecs);
			if (errorMsg == "")
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.OK);
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
			}

		}
		#endregion

		#endregion

		//***********************************	DELETE  ***********************************	

		#region DELETE

		#region tbl_Devices_Delete

		//qry_tbl_Devices_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblDevices/{keyValue}")]
		public HttpResponseMessage delete_tblPhones(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_Devices dataRecs = new HE_Models.Points.tbl_Devices();
				HE_Models.Points.tbl_Device dataRec = new HE_Models.Points.tbl_Device()
				{
					keyDeviceID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_PointUniqueValues_Delete

		//qry_tbl_PointUniqueValues_Delete ****DNE****
		[HttpDelete]
		[Route("api/V1/{environ}/tblPointUniqueValues/{keyValue}")]
		public HttpResponseMessage delete_tblPointUniqueValues(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
				HE_Models.Points.tbl_PointUniqueValue dataRec = new HE_Models.Points.tbl_PointUniqueValue()
				{
					keyPointUV_ID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointUniqueValues_DeleteField
		[HttpDelete]
		[Route("api/V1/{environ}/tblPointUniqueValues/DeleteField/{inFkPUDF_ID}")]
		public HttpResponseMessage delete_tblPointUniqueValues_DeleteField(string environ, string inFkPUDF_ID)
		{
			string sAction = "DELETEFIELD";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
				HE_Models.Points.tbl_PointUniqueValue dataRec = new HE_Models.Points.tbl_PointUniqueValue()
				{
					fkPUDF_ID = inFkPUDF_ID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointUniqueValues_DeletePoint
		[HttpDelete]
		[Route("api/V1/{environ}/tblPointUniqueValues/DeletePoint/{inFkPointID}")]
		public HttpResponseMessage delete_tblPointUniqueValues_DeletePoint(string environ, string inFkPointID)
		{
			string sAction = "DELETEPOINT";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_PointUniqueValues dataRecs = new HE_Models.Points.tbl_PointUniqueValues();
				HE_Models.Points.tbl_PointUniqueValue dataRec = new HE_Models.Points.tbl_PointUniqueValue()
				{
					fkPointID = inFkPointID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region tbl_Points_Delete

		//qry_tbl_Points_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblPoints/{keyValue}")]
		public HttpResponseMessage delete_tblPoints(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_Points dataRecs = new HE_Models.Points.tbl_Points();
				HE_Models.Points.tbl_Point dataRec = new HE_Models.Points.tbl_Point()
				{
					keyPointID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_PointRanges_Delete

		//qry_tbl_PointRanges_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblPointRanges/{keyValue}")]
		public HttpResponseMessage delete_tblPointRanges(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_PointRanges dataRecs = new HE_Models.Points.tbl_PointRanges();
				HE_Models.Points.tbl_PointRange dataRec = new HE_Models.Points.tbl_PointRange()
				{
					keyPointRangeID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_PointRanges_DeleteRanges_ForPoint
		[HttpDelete]
		[Route("api/V1/{environ}/tblPointRanges/DeleteForPoint/{inFkPointID}")]
		public HttpResponseMessage delete_tblPointRanges_ForPoint(string environ, string inFkPointID)
		{
			string sAction = "DELETERANGES_FORPOINT";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_PointRanges dataRecs = new HE_Models.Points.tbl_PointRanges();
				HE_Models.Points.tbl_PointRange dataRec = new HE_Models.Points.tbl_PointRange()
				{
					fkPointID = inFkPointID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_DeviceUniqueValues_Delete

		//qry_tbl_DeviceUniqueValues_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/{keyValue}")]
		public HttpResponseMessage delete_tblDeviceUniqueValues(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
				HE_Models.Points.tbl_DeviceUniqueValue dataRec = new HE_Models.Points.tbl_DeviceUniqueValue()
				{
					keyDevUV_ID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DeviceUniqueValues_DeleteDevice
		[HttpDelete]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/DeleteDevice/{inFkDeviceID}")]
		public HttpResponseMessage delete_tblDeviceUniqueValues_Device(string environ, string inFkDeviceID)
		{
			string sAction = "DELETEDEVICE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
				HE_Models.Points.tbl_DeviceUniqueValue dataRec = new HE_Models.Points.tbl_DeviceUniqueValue()
				{
					fkDeviceID = inFkDeviceID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_DeviceUniqueValues_DeleteField
		[HttpDelete]
		[Route("api/V1/{environ}/tblDeviceUniqueValues/DeleteField/{inFkDUDF_ID}")]
		public HttpResponseMessage delete_tblDeviceUniqueValues_Field(string environ, string inFkDUDF_ID)
		{
			string sAction = "DELETEFIELD";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_DeviceUniqueValues dataRecs = new HE_Models.Points.tbl_DeviceUniqueValues();
				HE_Models.Points.tbl_DeviceUniqueValue dataRec = new HE_Models.Points.tbl_DeviceUniqueValue()
				{
					fkDUDF_ID = inFkDUDF_ID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_DAQ_Delete

		//qry_tblDAQ_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblDAQ/{keyValue}")]
		public HttpResponseMessage delete_tblDAQ(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_DAQs dataRecs = new HE_Models.Points.tbl_DAQs();
				HE_Models.Points.tbl_DAQ dataRec = new HE_Models.Points.tbl_DAQ()
				{
					keyDAQ_ID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_Buses_Delete

		//qry_tbl_Buses_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblBuses/{keyValue}")]
		public HttpResponseMessage delete_tblBuses(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_Buses dataRecs = new HE_Models.Points.tbl_Buses();
				HE_Models.Points.tbl_Bus dataRec = new HE_Models.Points.tbl_Bus()
				{
					keyBusID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_tbl_Buses_DeleteAll_ForDAQ
		[HttpDelete]
		[Route("api/V1/{environ}/tblBuses/DeleteForDAQ/{inFkDAQ_ID}")]
		public HttpResponseMessage delete_tblBuses_ForDAQ(string environ, string inFkDAQ_ID)
		{
			string sAction = "DELETEALL_FORDAQ";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_Buses dataRecs = new HE_Models.Points.tbl_Buses();
				HE_Models.Points.tbl_Bus dataRec = new HE_Models.Points.tbl_Bus()
				{
					fkDAQ_ID = inFkDAQ_ID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_Versions_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refVersions/{keyValue}")]
		public HttpResponseMessage delete_refVersions(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_Versions dataRecs = new HE_Models.Points.ref_Versions();
				HE_Models.Points.ref_Version dataRec = new HE_Models.Points.ref_Version()
				{
					keyVersionID = Convert.ToInt32(keyValue)
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_Units_Delete

		//qry_ref_Units_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refUnits/{keyValue}")]
		public HttpResponseMessage delete_refUnits(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_Units dataRecs = new HE_Models.Points.ref_Units();
				HE_Models.Points.ref_Unit dataRec = new HE_Models.Points.ref_Unit()
				{
					keyUnitID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_TranslationValues_Delete

		//qry DNE
		[HttpDelete]
		[Route("api/V1/{environ}/refTranslatinoValues/{keyValue}")]
		public HttpResponseMessage delete_refTranslationValues(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_TranslationValues dataRecs = new HE_Models.Points.ref_TranslationValues();
				HE_Models.Points.ref_TranslationValue dataRec = new HE_Models.Points.ref_TranslationValue()
				{
					keyTranslationValueID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_TranslationValues_DeleteValues_ForTranslationType
		[HttpDelete]
		[Route("api/V1/{environ}/refTranslatinoValues/DeleteForType/{inKeyTranslationTypeID}")]
		public HttpResponseMessage delete_refTranslationValues_ForType(string environ, string inKeyTranslationTypeID)
		{
			string sAction = "DELETEFORTRANSLATIONTYPE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_TranslationValues dataRecs = new HE_Models.Points.ref_TranslationValues();
				HE_Models.Points.ref_TranslationValue dataRec = new HE_Models.Points.ref_TranslationValue()
				{
					fkTranslationTypeID = inKeyTranslationTypeID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_TranslationTypes_Delete

		//qry_ref_TranslationTypes_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refTranslationTypes/{keyValue}")]
		public HttpResponseMessage delete_refTranslationTypes(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_TranslationTypes dataRecs = new HE_Models.Points.ref_TranslationTypes();
				HE_Models.Points.ref_TranslationType dataRec = new HE_Models.Points.ref_TranslationType()
				{
					keyTranslationTypeID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_Recorders_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refRecorders/{keyValue}")]
		public HttpResponseMessage delete_refRecorders(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_Recorders dataRecs = new HE_Models.Points.ref_Recorders();
				HE_Models.Points.ref_Recorder dataRec = new HE_Models.Points.ref_Recorder()
				{
					keyRecorderID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_RangeTypes_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refRangeTypes/{keyValue}")]
		public HttpResponseMessage delete_refRangeTypes(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_RangeTypes dataRecs = new HE_Models.Points.ref_RangeTypes();
				HE_Models.Points.ref_RangeType dataRec = new HE_Models.Points.ref_RangeType()
				{
					keyRangeTypeID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_RangePresets_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refRangePresets/{keyValue}")]
		public HttpResponseMessage delete_refRangePresets(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_RangePresets dataRecs = new HE_Models.Points.ref_RangePresets();
				HE_Models.Points.ref_RangePreset dataRec = new HE_Models.Points.ref_RangePreset()
				{
					keyRangePresetID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_PointUDFs_Delete

		//qry DNE
		[HttpDelete]
		[Route("api/V1/{environ}/refPointUDFs/{keyValue}")]
		public HttpResponseMessage delete_refPointUDFs(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_PointUDFs dataRecs = new HE_Models.Points.ref_PointUDFs();
				HE_Models.Points.ref_PointUDF dataRec = new HE_Models.Points.ref_PointUDF()
				{
					keyPUDF_ID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_PointUDFs_DeleteFields_ForDeviceType
		[HttpDelete]
		[Route("api/V1/{environ}/refPointUDFs/DeleteFieldsForDeviceType/{inFkDevTypeID}")]
		public HttpResponseMessage delete_refPointUDFs_deleteFieldsForDevType(string inFkDevTypeID)
		{
			string sAction = "DELETEFIELDS_FORDEVICETYPE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_PointUDFs dataRecs = new HE_Models.Points.ref_PointUDFs();
				HE_Models.Points.ref_PointUDF dataRec = new HE_Models.Points.ref_PointUDF()
				{
					fkDevTypeID = inFkDevTypeID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP("Points", sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_DriverTypes_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refDriverTypes/{keyValue}")]
		public HttpResponseMessage delete_refDriverTypes(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_DriverTypes dataRecs = new HE_Models.Points.ref_DriverTypes();
				HE_Models.Points.ref_DriverType dataRec = new HE_Models.Points.ref_DriverType()
				{
					keyDriverTypeID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_DeviceUDFs_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refDeviceUDFs/{keyValue}")]
		public HttpResponseMessage delete_refDeviceUDFs(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_DeviceUDFs dataRecs = new HE_Models.Points.ref_DeviceUDFs();
				HE_Models.Points.ref_DeviceUDF dataRec = new HE_Models.Points.ref_DeviceUDF()
				{
					keyDUDF_ID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_ref_DeviceUDFs_DeleteFields_ForDeviceType
		[HttpDelete]
		[Route("api/V1/{environ}/refDeviceUDFs/DeleteFieldsForDevice/{inFkDevTypeID}")]
		public HttpResponseMessage delete_refDeviceUDFs_DeleteFieldsForDevice(string environ, string inFkDevTypeID)
		{
			string sAction = "DELETEFIELDS_FORDEVICETYPE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_DeviceUDFs dataRecs = new HE_Models.Points.ref_DeviceUDFs();
				HE_Models.Points.ref_DeviceUDF dataRec = new HE_Models.Points.ref_DeviceUDF()
				{
					fkDevTypeID = inFkDevTypeID
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region ref_DeviceTypes_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refDeviceTypes/{keyValue}")]
		public HttpResponseMessage delete_refDeviceTypes(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_DeviceTypes dataRecs = new HE_Models.Points.ref_DeviceTypes();
				HE_Models.Points.ref_DeviceType dataRec = new HE_Models.Points.ref_DeviceType()
				{
					keyDevTypeID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region ref_BusTypes_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refBusTypes/{keyValue}")]
		public HttpResponseMessage delete_refBusTypes(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_BusTypes dataRecs = new HE_Models.Points.ref_BusTypes();
				HE_Models.Points.ref_BusType dataRec = new HE_Models.Points.ref_BusType()
				{
					keyBusTypeID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region lst_RecorderPoints_Delete
		//qry_lst_RecorderPoints_Delete ****DNE****
		[HttpDelete]
		[Route("api/V1/{environ}/lstRecorderPoints/{keyValue}")]
		public HttpResponseMessage delete_lstRecorderPoints(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
				HE_Models.Points.lst_RecorderPoint dataRec = new HE_Models.Points.lst_RecorderPoint()
				{
					keyRecorderPointID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_lst_RecorderPoints_DeletePoints_ForRecorder
		[HttpDelete]
		[Route("api/V1/{environ}/lstRecorderPoints/DeletePointsForRecorder/{inFkRecorderID}")]
		public HttpResponseMessage delete_lstRecorderPoints_ForRecorder(string environ, string inFkRecorderID)
		{
			string sAction = "DeletePoints_ForRecorder";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
				HE_Models.Points.lst_RecorderPoint dataRec = new HE_Models.Points.lst_RecorderPoint()
				{
					fkRecorderID = inFkRecorderID
				};
				dataRecs.Add(dataRec);//if we only do recorder id we may have issue where pk is null

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_lst_RecorderPoints_DeleteRecorderPoint (From Body Version)
		[HttpDelete]
		[Route("api/V1/{environ}/lstRecorderPoints/DeleteRecorderPoint")]
		public HttpResponseMessage delete_lstRecorderPoints_RecorderPointFB(string environ, [FromBody()] HE_Models.Points.lst_RecorderPoint dataRec)
		{
			string sAction = "DeleteRecorderPoint";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
				/*
				HE_Models2.Points.lst_RecorderPoint dataRec = new HE_Models2.Points.lst_RecorderPoint()
				{
					fkRecorderID = inFkRecorderID
				};
				*/
				dataRecs.Add(dataRec); //now we should have dataRec being given a partially filled lst_RecorderPoint object with only fkRecorderID and fkPointID filled in

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_lst_RecorderPoints_DeleteRecorderPoint
		[HttpDelete]
		[Route("api/V1/{environ}/lstRecorderPoints/DeleteRecorderPoint/{inFkRecorderID}/{inFkPointID}")]
		public HttpResponseMessage delete_lstRecorderPoints_RecorderPoint(string inFkRecorderID, string inFkPointID)
		{
			string sAction = "DeleteRecorderPoint";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();
				HE_Models.Points.lst_RecorderPoint dataRec = new HE_Models.Points.lst_RecorderPoint()
				{
					fkRecorderID = inFkRecorderID,
					fkPointID = inFkPointID
				};
				
				dataRecs.Add(dataRec); //now we should have dataRec being given a partially filled lst_RecorderPoint object with only fkRecorderID and fkPointID filled in

				string errorMessage = "";
				errorMessage = oDB.processSP("Points", sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		//qry_lst_RecorderPoints_DeleteRecorders_ForPoint
		[HttpDelete]
		[Route("api/V1/{environ}/lstRecorderPoints/DeleteRecordersForPoint/{inFkPointID}")]
		public HttpResponseMessage delete_lstRecorderPoints_RecordersForPoint(string environ, string inFkPointID)
		{
			string sAction = "DeleteRecorders_ForPoint";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.lst_RecorderPoints dataRecs = new HE_Models.Points.lst_RecorderPoints();

				HE_Models.Points.lst_RecorderPoint dataRec = new HE_Models.Points.lst_RecorderPoint()
				{
					fkPointID = inFkPointID
				};
				
				dataRecs.Add(dataRec); //now we should have dataRec being given a partially filled lst_RecorderPoint object with only fkRecorderID and fkPointID filled in

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}
		#endregion

		#region ref_CalibrationValuesTypes

		//qry_ref_CalibrationValuesTypes_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/refCalibrationValuesTypes/{keyValue}")]
		public HttpResponseMessage delete_refCalibrationValuesTypes(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.ref_CalibrationValuesTypes dataRecs = new HE_Models.Points.ref_CalibrationValuesTypes();
				HE_Models.Points.ref_CalibrationValuesType dataRec = new HE_Models.Points.ref_CalibrationValuesType()
				{
					keyCalibrationValuesTypeID = Convert.ToInt32(keyValue)
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_CalibrationLists

		//qry_tbl_CalibrationLists_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblCalibrationLists/{keyValue}")]
		public HttpResponseMessage delete_tblCalibrationLists(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid) { 

				HE_Models.Points.tbl_CalibrationLists dataRecs = new HE_Models.Points.tbl_CalibrationLists();
				HE_Models.Points.tbl_CalibrationList dataRec = new HE_Models.Points.tbl_CalibrationList()
				{
					keyCalibrationListID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_CalibrationRows

		//qry_tbl_CalibrationRows_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblCalibrationRows/{keyValue}")]
		public HttpResponseMessage delete_tblCalibrationRows(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_CalibrationRows dataRecs = new HE_Models.Points.tbl_CalibrationRows();
				HE_Models.Points.tbl_CalibrationRow dataRec = new HE_Models.Points.tbl_CalibrationRow()
				{
					keyCalibrationRowID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_HistoricPointDatas

		//qry_tbl_HistoricPointDatas_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblHistoricPointDatas/{keyValue}")]
		public HttpResponseMessage delete_tblHistoricPointDatas(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_HistoricPointDatas dataRecs = new HE_Models.Points.tbl_HistoricPointDatas();
				HE_Models.Points.tbl_HistoricPointData dataRec = new HE_Models.Points.tbl_HistoricPointData()
				{
					keyHisoricPointData = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#region tbl_HistoricPointUniqueValues

		//qry_tbl_HistoricPointUniqueValues_Delete
		[HttpDelete]
		[Route("api/V1/{environ}/tblHistoricPointUniqueValues/{keyValue}")]
		public HttpResponseMessage delete_tbl_HistoricPointUniqueValues(string environ, string keyValue)
		{
			string sAction = "DELETE";
			Boolean bValid = true;
			if (bValid)
			{
				HE_Models.Points.tbl_HistoricPointUniqueValues dataRecs = new HE_Models.Points.tbl_HistoricPointUniqueValues();
				HE_Models.Points.tbl_HistoricPointUniqueValue dataRec = new HE_Models.Points.tbl_HistoricPointUniqueValue()
				{
					keyHistoricPointUniqueValueID = keyValue
				};
				dataRecs.Add(dataRec);

				string errorMessage = "";
				errorMessage = oDB.processSP(environ, sAction, dataRecs);
				if (errorMessage == "")
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.OK);
				}
				else
				{
					return Request.CreateResponse(System.Net.HttpStatusCode.BadRequest);
				}
			}
			else
			{
				return Request.CreateResponse(System.Net.HttpStatusCode.Unauthorized);
			}
		}

		#endregion

		#endregion
	}
}
