﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Mvc;

namespace MakeItWork.Controllers
{
	public class BasicsController : ApiController
	{
        /*
        #region "tblPhones"

        [HttpPost]
        private int post(HE_Models2.Points.tbl_Devices dataRecs, string userToken)
        {
            int result = -1;
            string tblName = "tblDevices";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:44399/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


                result = client.PostAsync("/api/V1/Points/" + tblName, dataRecs, new JsonMediaTypeFormatter()).Result.Content.ReadAsAsync<int>().Result;
            }
            return result;
        }
        [HttpPut]
        private int put(HE_Models2.Points.tbl_Devices dataRecs, string userToken)
        {
            int result = -1;
            string tblName = "tblDevices";
            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:44399/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                result = client.PutAsync("/api/V1/Points/" + tblName, dataRecs, new JsonMediaTypeFormatter()).Result.Content.ReadAsAsync<int>().Result;

            }
            return result;
        }
        [HttpDelete]
        private int delete(HE_Models2.Points.tbl_Devices dataRecs, string userToken)
        {
            int result = -1;
            string tblName = "tblDevices";

            string keyValue = "";
            foreach (HE_Models2.Points.tbl_Device dataRec in dataRecs)
            {

                keyValue = dataRec.keyDeviceID.ToString();
            }

            using (var client = new HttpClient())
            {
                client.BaseAddress = new System.Uri("http://localhost:44399/");
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                result = client.DeleteAsync("/api/V1/Points/" + tblName + '/' + keyValue).Result.Content.ReadAsAsync<int>().Result;

            }
            return result;
        }

        public ActionResult Process_tblDevices(FormCollection oForm)
        {
            Boolean bValid = false; string UTK = System.Guid.Empty.ToString();
            //if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("HE_UsrToken")) { UTK = this.ControllerContext.HttpContext.Request.Cookies["HE_UsrToken"].Value.ToString(); }
            //bValid = oFunc.validateUTK(UTK);
            //if (bValid)
            if (true)
            {
                // -----------------------------------------------------------
                //| Begin Processing
                // -----------------------------------------------------------
                Boolean phonesBolPrimary = false;
                try { if (oForm["phonesBolPrimary"].ToString() == "on") { phonesBolPrimary = true; } } catch { }

                HE_Models.Calibrations.tblPhones dataRecs = new HE_Models.Calibrations.tblPhones();
                HE_Models.Calibrations.tblPhone dataRec = new HE_Models.Calibrations.tblPhone();
                string viewAction = oForm["viewAction"].ToString();
                if (viewAction.ToUpper() == "ADD")
                {
                    dataRec.keyPhoneID = Guid.NewGuid().ToString();
                }
                else
                {
                    dataRec.keyPhoneID = oForm["keyPhoneID"];
                }
                dataRec.fkPersonID = oForm["keyPersonID"];
                dataRec.fkPhoneTypeID = Convert.ToInt32(oForm["keyPhoneTypeID"]);
                dataRec.phonesStrNumber = oForm["phonesStrNumber"].ToString();
                dataRec.phonesBolPrimary = phonesBolPrimary;

                dataRecs.Add(dataRec);

                string actionMsg = "";
                //string viewAction = oForm["viewAction"].ToString();
                int result = -1;
                switch (viewAction.ToUpper())
                {
                    case "ADD": result = post(dataRecs, UTK); actionMsg = "Insert Complete"; break;
                    case "CHANGE": result = put(dataRecs, UTK); actionMsg = "Update Complete"; break;
                    case "DELETE": result = delete(dataRecs, UTK); actionMsg = "Delete Complete"; break;
                }

                // -----------------------------------------------------------
                //| Begin Processing
                // -----------------------------------------------------------

                return RedirectToAction("tblPhones_List", "Basics", new { actionMsg = actionMsg });
            }
            else
            {
                return RedirectToAction("Logout", "Main", new { loginMessage = "You have been logged out due to inactivity." });
            }
        }
        public ActionResult tblPhones_View(string viewKey, string viewAction)
        {
            Boolean bValid = false; string UTK = System.Guid.Empty.ToString();
            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("HE_UsrToken")) { UTK = this.ControllerContext.HttpContext.Request.Cookies["HE_UsrToken"].Value.ToString(); }
            bValid = oFunc.validateUTK(UTK);
            if (bValid)
            {
                // -----------------------------------------------------------
                //| Begin Processing
                // -----------------------------------------------------------
                string URI = Models.FunctionsAndModels.apiServer + "/api/V1/Calibrations/Class/tblPhones/" + viewKey;
                HE_Models.Calibrations.tblPhones dataRecs = new HE_Models.Calibrations.tblPhones();
                dataRecs = apiFunc.getData(URI, System.Guid.Empty.ToString(), System.Guid.Empty.ToString(), dataRecs);
                ViewData["dataRecs"] = dataRecs;

                ViewData["viewKey"] = viewKey;
                ViewData["viewAction"] = viewAction;

                // -----------------------------------------------------------
                //| Begin Processing
                // -----------------------------------------------------------

                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Main", new { loginMessage = "You have been logged out due to inactivity." });
            }
        }
        public ActionResult tblPhones_List(string actionMsg)
        {
            Boolean bValid = false; string UTK = System.Guid.Empty.ToString();
            if (this.ControllerContext.HttpContext.Request.Cookies.AllKeys.Contains("HE_UsrToken")) { UTK = this.ControllerContext.HttpContext.Request.Cookies["HE_UsrToken"].Value.ToString(); }
            bValid = oFunc.validateUTK(UTK);
            if (bValid)
            {
                // -----------------------------------------------------------
                //| Begin Processing
                // -----------------------------------------------------------
                string URI = Models.FunctionsAndModels.apiServer + "/api/V1/Calibrations/Class/tblPhones";
                HE_Models.Calibrations.tblPhones dataRecs = new HE_Models.Calibrations.tblPhones();
                dataRecs = apiFunc.getData(URI, System.Guid.Empty.ToString(), System.Guid.Empty.ToString(), dataRecs);
                ViewData["dataRecs"] = dataRecs;

                if (actionMsg == null) { actionMsg = ""; }
                ViewData["actionMsg"] = actionMsg;
                // -----------------------------------------------------------
                //| Begin Processing
                // -----------------------------------------------------------

                return View();
            }
            else
            {
                return RedirectToAction("Logout", "Main", new { loginMessage = "You have been logged out due to inactivity." });
            }
        }

        #endregion
        */
    }
}